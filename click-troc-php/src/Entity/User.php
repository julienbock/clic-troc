<?php

namespace App\Entity;

use App\Request\ServiceRequest;
use App\Request\UserRequest;
use UserInterface;

class User
{
    protected $id;

    protected $username;

    protected $password;

    protected $firstname;

    protected $lastname;

    protected $email;

    protected $phone;

    protected $city;

    protected $hobbies;

    protected $avatar = [];

    protected $sr;

    protected $ur;

    protected $friends = [];

    public function __construct($datas = null, $getFriends = false)
    {
        if ($datas !== null) {
            foreach ($datas as $attribut => $value) {
                $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
                if (is_callable(array($this, $method))) {
                    $this->$method($value);
                }
            }
        }

        $this->sr = new ServiceRequest();
        $this->ur = new UserRequest();
        $results = $this->sr->findAvatarByUserId($this->getId());
        foreach ($results as $result) {
            $this->avatar[] = $result;
        }

        if ($getFriends) {
            $relations = $this->sr->findIdRelationFriendShip($this->getId());
            foreach ($relations as $relation) {
                $keyArrayCurrentUser = array_search($this->getId(), $relation);
                $friendId = null;
                if ($keyArrayCurrentUser == 'friend1') {
                    $friendId = $relation['friend2'];
                } else {
                    $friendId = $relation['friend1'];
                }

                $friend = $this->sr->findUserById((int)$friendId);
                $this->friends[] = $friend;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHobbies()
    {
        return $this->hobbies;
    }

    /**
     * @param mixed $hobbies
     * @return User
     */
    public function setHobbies($hobbies)
    {
        $this->hobbies = $hobbies;
        return $this;
    }

    /**
     * @return array
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param array $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return array
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * @param array $friends
     * @return User
     */
    public function setFriends($friends)
    {
        $this->friends = $friends;
        return $this;
    }
}