<?php

namespace App\Entity;

use App\Core\Manager\AbstractRequestManager;
use App\Request\ServiceRequest;
use DateTime;

class Announce extends AbstractRequestManager
{
    protected $id;

    protected $title;

    protected $description;

    protected $type;

    protected $upload_name;

    protected $user_id;

    protected $community_id;

    protected $created_at;

    protected $updated_at;

    protected $author;

    protected $uev_value;

    protected $waiting_exchange = false;

    protected $was_exchanged = false;

    protected $proposal = [];

    protected $images = [];

    protected $sr;

    public function __construct($ORM = true, $datas = null)
    {
        parent::__construct();

        if($datas !== null) {
            foreach ($datas as $attribut => $value) {
                $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
                if (is_callable(array($this, $method))) {
                    $this->$method($value);
                }
            }
        }

        $this->sr = new ServiceRequest();
        $results = $this->sr->findImageByAnnounceId($this->getId());
        foreach ($results as $result) {
            $this->images[] = $result;
        }

        if($ORM) {
            $this->author = $this->getUr()->findUserByIdWithoutPassword($this->getUserId());
            $proposals = $this->getPr()->findByAnnounce($this->getId());
            foreach ($proposals as $proposal) {
                $this->proposal[] = $proposal;
            }
        }

    }

    public function __set($name, $value)
    {
        $method = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
        if (is_callable(array($this, $method))) {
            $this->$method($value);
        }
    }


    private static function getSetter(string $setterName) {
        return 'set'.ucfirst($setterName);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Announce
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommunityId()
    {
        return $this->community_id;
    }

    /**
     * @param mixed $community_id
     * @return Announce
     */
    public function setCommunityId($community_id)
    {
        $this->community_id = $community_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Announce
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     * @return Announce
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUploadName()
    {
        return $this->upload_name;
    }

    /**
     * @param mixed $upload_name
     */
    public function setUploadName($upload_name)
    {
        $this->upload_name = $upload_name;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getUevValue()
    {
        return $this->uev_value;
    }

    /**
     * @param mixed $uev_value
     * @return Announce
     */
    public function setUevValue($uev_value)
    {
        $this->uev_value = (int)$uev_value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * @param mixed $proposal
     */
    public function setProposal($proposal)
    {
        $this->proposal = $proposal;
        return $this;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     * @return Announce
     */
    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWaitingExchange()
    {
        return $this->waiting_exchange;
    }

    /**
     * @param bool $waiting_exchange
     * @return Announce
     */
    public function setWaitingExchange($waiting_exchange)
    {
        $this->waiting_exchange = $waiting_exchange;
        return $this;
    }


    /**
     * @return bool
     */
    public function isWasExchanged()
    {
        return $this->was_exchanged;
    }

    /**
     * @param bool $was_exchanged
     * @return Announce
     */
    public function setWasExchanged($was_exchanged)
    {
        $this->was_exchanged = $was_exchanged;
        return $this;
    }
}