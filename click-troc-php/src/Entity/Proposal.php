<?php


namespace App\Entity;


use App\Request\UserRequest;

class Proposal
{

    protected $id;

    protected $user_id_proposal;

    protected $announce;

    protected $accepted_at;

    protected $rejected_at;

    protected $created_at;

    protected $user;

    protected $ur;

    public function __construct($datas = null)
    {
        if($datas !== null) {
            foreach ($datas as $attribut => $value) {
                $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
                if (is_callable(array($this, $method))) {
                    $this->$method($value);
                }
            }
        }

        if($this->getUserIdProposal()) {
            $this->ur = new UserRequest();
            $this->setUser($this->ur->findUserById($this->getUserIdProposal()));
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Proposal
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserIdProposal()
    {
        return $this->user_id_proposal;
    }

    /**
     * @param mixed $user_id_proposal
     * @return Proposal
     */
    public function setUserIdProposal($user_id_proposal)
    {
        $this->user_id_proposal = $user_id_proposal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * @param mixed $announce
     * @return Proposal
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcceptedAt()
    {
        return $this->accepted_at;
    }

    /**
     * @param mixed $accepted_at
     * @return Proposal
     */
    public function setAcceptedAt($accepted_at)
    {
        $this->accepted_at = $accepted_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRejectedAt()
    {
        return $this->rejected_at;
    }

    /**
     * @param mixed $rejected_at
     * @return Proposal
     */
    public function setRejectedAt($rejected_at)
    {
        $this->rejected_at = $rejected_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Proposal
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Proposal
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
}