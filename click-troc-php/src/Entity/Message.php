<?php

namespace App\Entity;

use App\Request\AnnounceRequest;
use App\Request\MessageRequest;
use App\Request\UserRequest;

class Message
{
    protected $id;

    protected $id2;

    protected $title;

    protected $sender_id;

    protected $receiver_id;

    protected $message;

    protected $created_at;

    protected $sender_read;

    protected $receiver_read;

    protected $announce_id = null;

    protected $sender;

    protected $receiver;

    protected $friendship_id = null;

    protected $has_discussion;

    protected $has_alert;

    protected $has_proposal;

    protected $announce;

    protected $receiverReadLastMessage = false;

    private $ar;

    private $ur;

    private $mr;

    public function __construct($relations = true, $datas = null, $getReceiverRead = false)
    {
        if($datas !== null) {
            foreach ($datas as $attribut => $value) {
                $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
                if (is_callable(array($this, $method))) {
                    $this->$method($value);
                }
            }
        }

        if($relations) {
            $this->ur = new UserRequest();
            $this->ar = new AnnounceRequest();
            $this->setSender($this->ur->findUserByIdWithoutPassword($this->getSenderId()));
            $this->setReceiver($this->ur->findUserByIdWithoutPassword($this->getReceiverId()));
            if ($this->getAnnounceId()) {
                $this->setAnnounce($this->ar->getAnnounceById($this->getAnnounceId()));
            }
        }

        if ($getReceiverRead) {
            $this->mr = new MessageRequest();
            $result = $this->mr->getReceiverReadLast($this->getId2());
            $this->setReceiverReadLastMessage(boolval($result['receiver_read']));
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Message
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId2()
    {
        return $this->id2;
    }

    /**
     * @param mixed $id2
     * @return Message
     */
    public function setId2($id2)
    {
        $this->id2 = $id2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderId()
    {
        return $this->sender_id;
    }

    /**
     * @param mixed $senderId
     * @return Message
     */
    public function setSenderId($senderId)
    {
        $this->sender_id = $senderId;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Message
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSenderRead()
    {
        return $this->sender_read;
    }

    /**
     * @param mixed $sender_read
     * @return Message
     */
    public function setSenderRead($sender_read)
    {
        $this->sender_read = $sender_read;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiverRead()
    {
        return $this->receiver_read;
    }

    /**
     * @param mixed $receiver_read
     * @return Message
     */
    public function setReceiverRead($receiver_read)
    {
        $this->receiver_read = $receiver_read;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnnounceId()
    {
        return $this->announce_id;
    }

    /**
     * @param mixed $announce_id
     * @return Message
     */
    public function setAnnounceId($announce_id)
    {
        $this->announce_id = $announce_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiverId()
    {
        return $this->receiver_id;
    }

    /**
     * @param mixed $receiverId
     */
    public function setReceiverId($receiverId)
    {
        $this->receiver_id = $receiverId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param mixed $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getFriendshipId()
    {
        return $this->friendship_id;
    }

    /**
     * @param mixed $friendship_id
     * @return Message
     */
    public function setFriendshipId($friendship_id)
    {
        $this->friendship_id = $friendship_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasDiscussion()
    {
        return $this->has_discussion;
    }

    /**
     * @param mixed $has_discussion
     * @return Message
     */
    public function setHasDiscussion($has_discussion)
    {
        $this->has_discussion = $has_discussion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasAlert()
    {
        return $this->has_alert;
    }

    /**
     * @param mixed $has_alert
     * @return Message
     */
    public function setHasAlert($has_alert)
    {
        $this->has_alert = $has_alert;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasProposal()
    {
        return $this->has_proposal;
    }

    /**
     * @param mixed $has_proposal
     * @return Message
     */
    public function setHasProposal($has_proposal)
    {
        $this->has_proposal = $has_proposal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAnnounce()
    {
        return $this->announce;
    }

    /**
     * @param mixed $announce
     * @return Message
     */
    public function setAnnounce($announce)
    {
        $this->announce = $announce;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReceiverReadLastMessage()
    {
        return $this->receiverReadLastMessage;
    }

    /**
     * @param bool $receiverReadLastMessage
     * @return Message
     */
    public function setReceiverReadLastMessage($receiverReadLastMessage)
    {
        $this->receiverReadLastMessage = $receiverReadLastMessage;
        return $this;
    }
}