<?php

namespace App\Entity;

use App\Request\CommunityRequest;
use App\Request\ServiceRequest;
use DateTime;

class Community
{
    protected $id;

    protected $name;

    protected $description;

    protected $admin;

    protected $city;

    protected $created_at;

    protected $upload_name = [];

    protected $images = [];

    protected $members;

    private $sr;

    private $cr;

    public function __construct($datas = null, $getMembers = false)
    {
        if($datas !== null) {
            foreach ($datas as $attribut => $value) {
                $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
                if (is_callable(array($this, $method))) {
                    $this->$method($value);
                }
            }
        }

        $this->sr = new ServiceRequest();
        $results = $this->sr->findImageByCommunityId($this->id);
        foreach ($results as $result) {
            $this->images[] = $result;
        }

        if ($getMembers) {
            $this->cr = new CommunityRequest();
            $this->setMembers($this->cr->getAllMembersOfCommunity($this->getId()));
        }
    }

    public function __set($name, $value)
    {
        $method = 'set'.str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
        if (is_callable(array($this, $method))) {
            $this->$method($value);
        }
    }


    private static function getSetter(string $setterName) {
        return 'set'.ucfirst($setterName);
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Community
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Community
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     * @return Community
     */
    public function setAdmin($admin)
    {
        $this->admin = (int)$admin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Community
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     * @return Community
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = (int)$city;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param mixed $members
     * @return Community
     */
    public function setMembers($members)
    {
        $this->members = $members;
        return $this;
    }
}