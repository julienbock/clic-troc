<?php

namespace App\Entity;

class Friendships
{

    protected $id;

    protected $friend1;

    protected $friend2;

    protected $created_at;

    protected $accepted_at;

    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFriend1()
    {
        return $this->friend1;
    }

    /**
     * @param mixed $friend1
     * @return Friendships
     */
    public function setFriend1($friend1)
    {
        $this->friend1 = $friend1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFriend2()
    {
        return $this->friend2;
    }

    /**
     * @param mixed $friend2
     * @return Friendships
     */
    public function setFriend2($friend2)
    {
        $this->friend2 = $friend2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return Friendships
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcceptedAt()
    {
        return $this->accepted_at;
    }

    /**
     * @param mixed $accepted_at
     * @return Friendships
     */
    public function setAcceptedAt($accepted_at)
    {
        $this->accepted_at = $accepted_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Friendships
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}