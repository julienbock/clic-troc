<?php

namespace App\Entity;

class SubscriptionPayment
{
    protected $id;

    protected $payedAt;

    protected $expireAt;

    protected $active;

    protected $user_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPayedAt()
    {
        return $this->payedAt;
    }

    /**
     * @param mixed $payedAt
     * @return SubscriptionPayment
     */
    public function setPayedAt($payedAt)
    {
        $this->payedAt = $payedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * @param mixed $expireAt
     * @return SubscriptionPayment
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     * @return SubscriptionPayment
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return SubscriptionPayment
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }
}