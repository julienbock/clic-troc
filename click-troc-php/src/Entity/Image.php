<?php

namespace App\Entity;

use Framework\Upload;

class Image
{
    protected $upload_name;

    protected $numOrder;

    /**
     * @return mixed
     */
    public function getUploadName()
    {
        return $this->upload_name;
    }

    /**
     * @param mixed $uploadName
     */
    public function setUploadName($upload_name)
    {
        $this->upload_name = $upload_name;
    }

    /**
     * @return mixed
     */
    public function getNumOrder()
    {
        return $this->numOrder;
    }

    /**
     * @param mixed $numOrder
     */
    public function setNumOrder($numOrder)
    {
        $this->numOrder = $numOrder;
    }


}