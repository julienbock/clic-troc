<?php

namespace App\Request;

use App\Core\Entity\DataBase;
use App\Core\Entity\QueryBuilder;

class AbstractRequest
{

    public function createQueryBuilder()
    {
        return new QueryBuilder();
    }
}