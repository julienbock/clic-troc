<?php

namespace App\Request;

use App\Entity\Message;

class MessageRequest extends AbstractRequest
{

    public function sendMessage($message)
    {
        $qb = $this->createQueryBuilder();

        $fields = [
            'id2',
            'title',
            'message',
            'sender_id',
            'receiver_id',
            'created_at',
            'sender_read',
            'receiver_read',
            'has_discussion',
            'has_alert',
            'has_proposal'
        ];

        $values = [
            $message->getId2(),
            $message->getTitle(),
            $message->getMessage(),
            $message->getSenderId(),
            $message->getReceiverId(),
            $message->getCreatedAt(),
            $message->getSenderRead(),
            $message->getReceiverRead(),
            (int)$message->getHasDiscussion(),
            (int)$message->getHasAlert(),
            (int)$message->getHasProposal()
        ];

        if($message->getFriendshipId()) {
            array_push($fields, 'friendship_id');
            array_push($values, $message->getFriendshipId());
        }

        if($message->getAnnounceId()) {
            array_push($fields, 'announce_id');
            array_push($values, $message->getAnnounceId());
        }

        $qb
            ->insert('messages', $fields, $values);

        return $qb->executeAndReturnLastId('messages');
    }

    public function updateNewMessageWithSameId($idMessageReturned)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('messages')
            ->set('id2', $idMessageReturned)
            ->where('id', $idMessageReturned);

        return $qb->execute();
    }

    public function findIdDiscussion($idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('id2')
            ->from('messages')
            ->where('receiver_id', $idUser)
            ->groupBy('id2');

        return $qb->fetchAll();
    }

    public function findPrivateMessagesByUser($idUser, $idMessages)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('messages')
            ->whereIn('id2', $idMessages)
            ->columnEqual('id2', 'id')
            ->addSQL('AND (sender_id = '.$idUser.' OR receiver_id = '.$idUser.')');

        return $qb->fetchAll(Message::class, [true, null, true]);
    }

    public function findDiscussionByUserAndMessage($idUser, $idMessage)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('messages')
            ->where('id', $idMessage)
            ->orWhere('id2', $idMessage)
            ->orderBy('created_at', 'ASC');

        return $qb->fetchAll(Message::class);
    }

    public function setReadedMessage($idUser, $idMessage)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('messages')
            ->set('receiver_read', 1)
            ->where('id', $idMessage)
            ->orWhere('id2', $idMessage)
            ->andWhere('receiver_id', $idUser)
            ->andWhere('receiver_read', 0);

        return $qb->execute();
    }

    public function findMessageById($idMessage)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('messages')
            ->where('id', $idMessage);

        return $qb->fetchObj(Message::class);
    }

    public function getReceiverReadLast($discussionId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('receiver_read')
            ->from('messages')
            ->where('id2', $discussionId)
            ->orderBy('created_at', 'DESC')
            ->limit(1);

        return $qb->fetch();
    }
}