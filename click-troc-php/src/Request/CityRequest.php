<?php

namespace App\Request;

use App\Entity\City;

class CityRequest extends AbstractRequest
{

    public function findCityByName($name)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('cities')
            ->like('name', $name)
            ->limit(5);
        return $qb->getResults();
    }

    public function findCityById($id)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('cities')
            ->where('id', $id);
        return $qb->fetchObj(City::class);
    }
}