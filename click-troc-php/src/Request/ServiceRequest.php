<?php

namespace App\Request;

use App\Entity\Image;
use App\Entity\User;
use DateTime;

class ServiceRequest extends AbstractRequest
{

    public function insetImageWithCommunityId($communityId, $image)
    {

        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('media_community', [
                'community_id',
                'upload_name',
                'num_order',
                'created_at'
            ], [
                $communityId,
                $image->getUploadName(),
                $image->getNumOrder(),
                $date->format('Y-m-d H:i:s')
            ])
            ->executeInsert();

            return true;
    }

    public function insetImageWithAnnounceId($announceId, $image)
    {

        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('media_announce', [
                'announce_id',
                'upload_name',
                'num_order',
                'created_at'
            ], [
                $announceId,
                $image->getUploadName(),
                $image->getNumOrder(),
                $date->format('Y-m-d H:i:s')
            ])
            ->executeInsert();

        return true;
    }

    public function insertImageWithUserId($userId, $image)
    {

        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('media_avatar', [
                'user_id',
                'upload_name',
                'num_order',
                'created_at'
            ], [
                $userId,
                $image->getUploadName(),
                $image->getNumOrder(),
                $date->format('Y-m-d H:i:s')
            ]);

        $qb->executeInsert();

        return true;
    }

    public function findAvatarByUserId($idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('media_avatar')
            ->where('user_id', $idUser);

        return $qb->fetchAll(Image::class);
    }

    public function findImageByAnnounceId($idAnnounce)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('media_announce')
            ->where('announce_id', $idAnnounce);

        return $qb->fetchAll(Image::class);
    }

    public function findImageByCommunityId($idCommunity)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('media_community')
            ->where('community_id', $idCommunity);

        return $qb->fetchAll(Image::class);
    }

    public function findIdRelationFriendShip($userId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('friend1, friend2')
            ->from('friendships')
            ->where('friend1', $userId)
            ->orWhere('friend2', $userId);

        return $qb->getResults();
    }


    public function findUserById($idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('user')
            ->where('id', $idUser);

        return $qb->fetchObj(User::class);
    }

}