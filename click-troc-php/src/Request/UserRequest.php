<?php

namespace App\Request;

use App\Entity\Friendships;
use App\Entity\SubscriptionPayment;
use App\Entity\User;

class UserRequest extends AbstractRequest
{
    public function createNewUser($table, Array $fields, Array $values)
    {
        $qb = $this->createQueryBuilder();

        $qb->insert($table, $fields, $values);

        return $qb->executeAndReturnLastId('user');
    }

    public function checkPaymentValidityByUser($userId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('subscription_payment')
            ->where('user_id', $userId)
            ->andWhere('active', 1);

        return $qb->fetchObj(SubscriptionPayment::class);
    }

    public function disablePaymentSubscription($subscriptionPaymentId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('subscription_payment')
            ->set('active', 0)
            ->where('id', $subscriptionPaymentId);

        return $qb->execute();
    }

    public function findUserByEmail($email)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('user')
            ->where('email', $email);

        return $qb->fetchObj(User::class);
    }


    public function findUserByName($username)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('user')
            ->where('username', $username);

        return $qb->getResults();
    }

    public function findUserById($idUser, $getFriend = false)
    {
        $qb = $this->createQueryBuilder();

        $qb->select('*')
            ->from('user')
            ->where('id', $idUser);

        return $qb->fetchObj(User::class, [null, $getFriend]);
    }

    public function findUserByIdWithoutPassword($idUser)
    {
        $qb = $this->createQueryBuilder();
        $qb->select('user.id, user.username, user.firstname, user.lastname, user.email')
            ->from('user')
            ->where('id', $idUser);

        return $qb->fetchObj(User::class);
    }

    public function updateUserProfile($idUser, $informationsEdited)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('user')
            ->setArray($informationsEdited)
            ->where('id', $idUser);

        return $qb->execute();
    }

    public function findMessageUnreadUser($idUser) {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('COUNT(*)')
            ->from('messages')
            ->where('receiver_id', $idUser)
            ->andWhere('receiver_read', 0);

        return $qb->fetchColumn();
    }

    public function checkFriendship($idUser1, $currentUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('friendships')
            ->addSQL(' WHERE (friend1 = '.$idUser1.' AND friend2 = '.$currentUser.') OR (friend1 = '.$currentUser.' AND friend2 = '.$idUser1.')');

        return $qb->fetchObj(Friendships::class);
    }

    public function replyFriendRequest($idFriendship, $reply)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('friendships')
            ->set('status', $reply)
            ->where('id', $idFriendship);

        return $qb->execute();
    }

    public function addPayment($payment)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('subscription_payment', ['payedAt', 'expireAt', 'active', 'user_id'], [$payment->getPayedAt(), $payment->getExpireAt(), $payment->getActive(), $payment->getUserId()]);

        $qb->executeAndReturnLastId('subscription_payment');
    }
}