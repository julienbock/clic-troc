<?php

namespace App\Request;

use App\Entity\Community;
use App\Entity\User;
use DateTime;

class CommunityRequest extends AbstractRequest
{
    public function requestGetMostActiveCommunities()
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('*')
            ->from('communities')
            ->limit(5);

        return $qb->fetchAll(Community::class);
    }

    public function findCommunityById($idCommunity)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('communities.*, media_community.upload_name, media_community.num_order')
            ->from('communities')
            ->leftJoin('media_community', 'community_id = communities.id')
            ->join('cities','id = communities.city')
            ->where('communities. id', $idCommunity);
        return $qb->fetchObj(Community::class, [null, true]);
    }

    public function findCommunityByCityId($cityId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('communities')
            ->where('communities.city', $cityId);

        return $qb->fetchAll(Community::class);
    }

    public function findCommunityByUser($idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('communities.*')
            ->from('communities')
            ->join('communities_user','community_id = communities.id')
            ->where('communities_user.user_id', $idUser);
        return $qb->fetchAll(Community::class);
    }

    public function addUserInCommunity($idUser, $idCommunity)
    {
        $exist= $this->createQueryBuilder();

        $exist
            ->select('*')
            ->from('communities_user')
            ->where('community_id', $idCommunity)
            ->andWhere('user_id', $idUser);

        if ($exist->fetchColumn() === false) {
            $qb = $this->createQueryBuilder();

            $qb->insert('communities_user', ['community_id', 'user_id'], [$idCommunity, $idUser]);
            $qb->executeInsert();

            return true;
        }

        return false;
    }

    public function removeUserInCommunity($idUser, $idCommunity)
    {
        $exist= $this->createQueryBuilder();

        $exist
            ->select('*')
            ->from('communities_user')
            ->where('community_id', $idCommunity)
            ->andWhere('user_id', $idUser);

        if ($exist->fetchColumn() == true) {
            $qb = $this->createQueryBuilder();

            $qb->delete('communities_user')
                ->where('community_id', $idCommunity)
                ->andWhere('user_id', $idUser);

            $qb->execute();

            return true;
        }

        return false;
    }

    public function isUserMember($idUser, $idCommunity)
    {
        $exist= $this->createQueryBuilder();

        $exist
            ->select('*')
            ->from('communities_user')
            ->where('community_id', $idCommunity)
            ->andWhere('user_id', $idUser);

        return $exist->fetchColumn();
    }

    public function addCommunity($community)
    {
        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('communities', [
                'name',
                'description',
                'admin',
                'city',
                'created_at'
            ], [
                $community->getName(),
                $community->getDescription(),
                $community->getAdmin(),
                $community->getCity(),
                $date->format('Y-m-d H:i:s')
            ]);

        return $qb->executeAndReturnLastId('communities');
    }

    public function getAllMembersOfCommunity($communityId)
    {
        $getIdMembers = $this->createQueryBuilder();

        $getIdMembers
            ->select('user_id')
            ->from('communities_user')
            ->where('community_id', $communityId);

        $allUserId = $getIdMembers->fetchAll();

        $members = [];
        foreach ($allUserId as $userId) {
            $qb = $this->createQueryBuilder();

            $qb
                ->select('*')
                ->from('user')
                ->where('id', $userId['user_id']);

            $members[] = $qb->fetchObj(User::class);
        }

        return $members;
    }
}