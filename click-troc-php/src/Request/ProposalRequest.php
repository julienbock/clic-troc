<?php


namespace App\Request;


use App\Entity\Proposal;
use DateTime;

class ProposalRequest extends AbstractRequest
{
    public function createNewProposal($table, Array $fields, Array $values)
    {
        $qb = $this->createQueryBuilder();

        $qb->insert($table, $fields, $values);

        return $qb->executeAndReturnLastId('proposal');
    }

    public function findByAnnounce($announceId)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('*')
            ->from('proposal')
            ->where('announce', $announceId);

        return $qb->fetchAll(Proposal::class);
    }

    public function findById($proposalId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('proposal')
            ->join('announces', 'id = proposal.announce')
            ->where('proposal.id', $proposalId);

        return $qb->fetchObj(Proposal::class);
    }

    public function acceptProposal($proposalId, $choiceValue)
    {
        $qb = $this->createQueryBuilder();
        $qb->update('proposal');

        if ($choiceValue) {
            $qb
                ->set('accepted_at', (new Datetime())->format('Y-m-d H:i:s'))
                ->andSet('rejected_at', 'null');
        } else {
            $qb
                ->set('rejected_at', (new Datetime())->format('Y-m-d H:i:s'))
                ->andSet('accepted_at', 'null');
        }

        $qb->where('id', $proposalId);

        return $qb->execute();
    }

    public function checkIfProposalAccepted($announceId)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->select('COUNT(id)')
            ->from('proposal')
            ->where('announce', $announceId)
            ->andWhereNotNull('accepted_at');

        return $qb->fetchColumn();
    }

    public function findByAnnounceIdAndAccepted($announceId)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('*')
            ->from('proposal')
            ->where('proposal.announce', $announceId)
            ->andWhereNotNull('accepted_at');

        return $qb->fetchObj(Proposal::class);
    }
}