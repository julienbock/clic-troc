<?php

namespace App\Request;

use App\Entity\Announce;
use App\Entity\City;
use DateTime;

class AnnounceRequest extends AbstractRequest
{
    public function insertAnnounce($announce)
    {
        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('announces', [
                'title',
                'description',
                'user_id',
                'community_id',
                'created_at',
                'type',
                'uev_value',
                'waiting_exchange',
                'was_exchanged'

            ], [
                $announce->getTitle(),
                $announce->getDescription(),
                $announce->getUserId(),
                $announce->getCommunityId(),
                $date->format('Y-m-d H:i:s'),
                $announce->getType(),
                $announce->getUevValue(),
                $announce->isWaitingExchange(),
                $announce->isWasExchanged()

            ]);

        return $qb->executeAndReturnLastId('communities');
    }

    public function getAnnouncesByCommunityId($idCommunity)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('announces.*')
            ->from('announces')
            ->where('community_id', $idCommunity)
            ->andWhere('waiting_exchange', 0);

        return $qb->fetchAll(Announce::class);
    }

    public function getAnnounceById($idAnnounce)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('announces.*, media_announce.upload_name')
            ->from('announces')
            ->leftJoin('media_announce', 'announce_id = announces.id')
            ->where('announces.id', $idAnnounce);

        return $qb->fetchObj(Announce::class);
    }

    public function getAnnouncesByUser($idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('announces.*')
            ->from('announces')
            ->where('user_id', $idUser  );

        return $qb->fetchAll(Announce::class);
    }


    public function deleteAnnounceByIdAndUserId($idAnnounce, $idUser)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->delete('announces')
            ->where('id', $idAnnounce)
            ->andWhere('user_id', $idUser);

        dump($qb->getQuerySQL());
        return $qb->execute();
    }


    public function deleteAnnounceByIdAndCommunityId($idAnnounce, $idCommunity)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->delete('announces')
            ->where('id', $idAnnounce)
            ->andWhere('community_id', $idCommunity);

        return $qb->execute();
    }

    public function findCityByAnnounce($idAnnounce, $idCommunity)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->select('cities.*')
            ->from('announces')
            ->leftJoin('communities', 'id = announces.community_id')
            ->leftJoin('cities', 'id = communities.city')
            ->where('announces.id', $idAnnounce)
            ->andWhere('announces.community_id', $idCommunity);

        return $qb->fetchObj(City::class);
    }

    public function removeAnnounceById($idAnnounce){
        $exist= $this->createQueryBuilder();

        $exist
            ->select('*')
            ->from('announces')
            ->where('id', $idAnnounce);

        if ($exist->fetchColumn() == true) {
            $qb = $this->createQueryBuilder();

            $qb->delete('announces')
                ->where('id', $idAnnounce);

            $qb->execute();

            return true;
        }

        return false;
    }

    public function setWaitingExchangeStatus($idAnnounce)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('announces')
            ->set('waiting_exchange', 1)
            ->where('id', $idAnnounce)
            ->andWhere('waiting_exchange', 0)
            ->andWhere('was_exchanged', 0);

        return $qb->execute();
    }

    public function setExchangedStatus($idAnnounce)
    {
        $qb = $this->createQueryBuilder();

        $qb
            ->update('announces')
            ->set('was_exchanged', 1)
            ->where('id', $idAnnounce)
            ->andWhere('waiting_exchange', 1)
            ->andWhere('was_exchanged', 0);

        return $qb->execute();
    }
}
