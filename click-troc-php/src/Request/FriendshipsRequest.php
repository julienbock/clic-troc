<?php

namespace App\Request;

use DateTime;

class FriendshipsRequest extends AbstractRequest
{


    public function insertRequestToFriend($idFriendSender, $idFriendRecever)
    {
        $date = new DateTime();
        $qb = $this->createQueryBuilder();

        $qb
            ->insert('friendships', [
                'friend1',
                'friend2',
                'created_at',
                'status'
            ], [
                $idFriendSender,
                $idFriendRecever,
                $date->format('Y-m-d H:i:s'),
                (int)false
                ]);

        return $qb->executeAndReturnLastId('friendships');
    }
}