<?php


namespace App\MonkeyPhp;


use App\Model\Wallet;
use DateTime;
use Symfony\Component\Yaml\Yaml;

class YAMLWallet
{

    protected $walletYAML;

    public static function createWalletYAML($userId)
    {
        $date = new DateTime();

        $array = [
            [
                'date' => $date->format('d/m/Y H:m:s'),
                'value' => 150,
                'type' => 'inscription',
                'total' => 150
            ]
        ];

        $file = YAML::dump($array);

        file_put_contents('../wallet/user'.$userId.'.yaml', $file);
    }

    public static function updateWalletYAML($userId, $type, $UEVvalue)
    {
        $date = new DateTime();

        $lastWallet = YAML::parseFile('../wallet/user'.$userId.'.yaml');

        $total = end($lastWallet)['total'] + ($UEVvalue);

        $informationToUpdateWallet = [
            [
                'date' => $date->format('d/m/Y H:m:s'),
                'value' => $UEVvalue,
                'type' => $type,
                'total' => $total
            ]
        ];

        file_put_contents('../wallet/user'.$userId.'.yaml', YAML::dump(array_merge_recursive($lastWallet, $informationToUpdateWallet)));

    }

    public static function readWalletYAML($userId)
    {
        $walletsValue = [];
        $values = YAML::parseFile('../wallet/user'.$userId.'.yaml');
        $valuesLimit = array_slice($values, count($values) - 10, count($values));
        foreach ($valuesLimit as $value)
        {
            $walletsValue[] = new Wallet($value);
        }

        return $walletsValue;
    }

    public static function getLastTotalWallet($userId)
    {
        $lastWallet = YAML::parseFile('../wallet/user'.$userId.'.yaml');

        return end($lastWallet)['total'];
    }
}