<?php

namespace Framework;

use App\MonkeyPhp\YAMLParameter;
use App\MonkeyPhp\YAMLRoute;
use Framework\Router\Route;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use http\Env\Request;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Route as ZendRoute;

class Router
{

    /*
     * @var RouteCollection()
     */
    private $router;

    private $base_uri;

    public function __construct()
    {
        $this->base_uri = YAMLParameter::getBaseUri();
        $this->router = new FastRouteRouter();
    }

    public function readRouteFromYAML($class)
    {
        $reflection = new ReflectionClass($class);
        $callableClassExist = [];
        foreach ($reflection->getMethods() as $methodClass) {
            $callableClassExist[] = $methodClass->getName();
        }

        $routeCollection = YAMLRoute::getRouteCollectionFromYAMLFile();
        foreach ($routeCollection as $route) {
            if (in_array($route->getCallable(), $callableClassExist)) {
                $method = (string)$route->getMethod();
                $this->$method($route->getPath(), [$class, $route->getCallable()], $route->getName());
            }
        }
    }

    public function get(string $path, $callable, string $name)
    {
        $this->router->addRoute(new ZendRoute($path, new MiddlewareApp($callable), ['GET'], $name));
    }

    public function post(string $path, $callable, string $name)
    {
        $this->router->addRoute(new ZendRoute($path, new MiddlewareApp($callable), ['POST'], $name));
    }

    public function match(ServerRequestInterface $request): ?Route
    {
        $result = $this->router->match($request);

        if ($result->isSuccess()) {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedRoute()->getMiddleware()->getCallable(),
                $result->getMatchedParams()
            );
        }

        return null;
    }

    public function generateUri(string $name, array $params = []): ?string
    {
        return $this->router->generateUri($name, $params);
    }

    public function redirect(string $name, $params, $httpCode = 301 )
    {
        $params = $params ? $params : [];

        $redirect = (new Response())->withStatus($httpCode)->withHeader('Location', $this->base_uri.$this->generateUri($name, $params));

        return $redirect;
    }
}