<?php
namespace Framework;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashExtension extends AbstractExtension {

    public function getFunctions(){
        return [new TwigFunction('flash', [$this, 'getFlashType'])];
    }

    public static function getFlashType(){
        $flash = $_SESSION['flash'];
        unset($_SESSION['flash']);
        if(!empty($flash)){
            return $flash['message'];
        }
        return null;
    }


}