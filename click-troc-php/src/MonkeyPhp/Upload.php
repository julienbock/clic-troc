<?php

namespace Framework;

use App\Entity\Image;
use App\Request\ServiceRequest;

class Upload
{
    const TYPE_COMMUNITY = "community";
    const TYPE_ANNOUNCE = "announce";
    const TYPE_USER = "avatar";
    public $exts = ["png", "jpg", "jpeg"]; //all the extensions that will be allowed to be uploaded
    public $maxSize = 9999999; //if you set to "0" (no quotes), there will be no limit
    public $uploadTarget = "uploads"; //make sure you have the '/' at the end
    public $fileName = ""; //this will be automatically set. you do not need to worry about this
    public $tmpName = ""; //this will be automatically set. you do not need to worry about this
    public $type;

    protected $serviceRequest;

    public function __construct($type)
    {
        $this->type = $type;
        $this->serviceRequest = new ServiceRequest();
    }

    public function startUpload($relationId)
    {
        if ($this->isWritable()) {
            foreach ($_FILES['file']['tmp_name'] as $key => $tmp_name) {
                $ext = pathinfo($_FILES['file']['name'][$key], PATHINFO_EXTENSION);
                $this->setFileName(uniqid(rand()).'.'.$ext);
                if($this->hasValidExtensions($ext)
                && $this->hasValidSize($_FILES['file']['size'][$key])) {
                    if(move_uploaded_file($_FILES['file']['tmp_name'][$key], $this->getPathUploadTarget().$this->getFileName())) {

                        $image = new Image();
                        $image->setUploadName($this->getFileName());
                        $image->setNumOrder($key);

                        if($this->type == self::TYPE_COMMUNITY) {
                            $this->serviceRequest->insetImageWithCommunityId($relationId, $image);
                        } elseif ($this->type == self::TYPE_ANNOUNCE) {
                            $this->serviceRequest->insetImageWithAnnounceId($relationId, $image);
                        } elseif ($this->type == self::TYPE_USER) {
                            $this->serviceRequest->insertImageWithUserId($relationId, $image);
                        }
                    }
                }
            }
        }
    }

    public function getPathUploadTarget()
    {
        return $this->uploadTarget.'/'.$this->type.'/';
    }

    public function isWritable()
    {
        return is_writable($this->getPathUploadTarget());
    }

    public function hasValidExtensions($ext)
    {
        return in_array($ext, $this->exts);
    }

    public function hasValidSize($size)
    {
        return ($size > $this->maxSize ) ? false : true;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}