<?php

namespace Framework;

use App\Manager\CommunityManager;
use App\MonkeyPhp\YAMLParameter;
use App\Request\CommunityRequest;
use Exception;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class App
{
    protected $twig;

    private $modules = [];

    protected $yamlLoader;

    /**
     * @var Router
     */
    private $router;

    public function __construct(array $modules = [])
    {
        $this->router = new Router();
        foreach ($modules as $module)
        {
            $this->modules[] = new $module($this->router);
        }
        $this->twig = new Twig();
        $this->yamlLoader = new YAMLParameter();
    }

    public function run(ServerRequestInterface $request)
    {
        $uri = $request->getUri()->getPath();

        if (!empty($uri) && ($uri === "/" || $uri[-1] === "/")) {
            return $this->router->redirect('homepage', null, 301);
        }

        $route = $this->router->match($request);

        if (is_null($route))
        {
            return new Response(404, [null], 'Error 404');
        }

        $params = $route->getParameters();

        $request = array_reduce(array_keys($params), function ($request, $key) use ($params) {
            return $request->withAttribute($key, $params[$key]);
        }, $request);

        $response = call_user_func_array($route->getCallback(), [$request]);

        if (is_string($response))
        {
            return new Response(200, [null], $response);
        } elseif ($response instanceOf ResponseInterface) {
            return $response;
        } else {
            throw new Exception('The response is not a string o instance of response interface');
        }
    }
}