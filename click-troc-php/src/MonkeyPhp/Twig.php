<?php

namespace Framework;

use Plasticbrain\FlashMessages\FlashMessages;
use Stripe\Checkout\Session;
use Twig\Extension\DebugExtension;
use Twig\TwigFunction;
use Twig_Environment;
use Twig_Extensions_Extension_Text;
use Twig_Loader_Filesystem;

class Twig {

    private $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../../templates');
        $this->twig = new Twig_Environment($loader, ['debug' => true]);
        $this->twig->addExtension(new DebugExtension());
        $this->twig->addExtension(new ClickTrocTwigExtension());
        $this->twig->addExtension(new FlashExtension());
        $this->twig->addExtension(new Twig_Extensions_Extension_Text());
    }

    public function render($view, $params  = [])
    {
        return $this->twig->render($view, $params);
    }

    public function addGlobal($name, $value)
    {
        $this->twig->addGlobal($name, $value);
    }

    public function addExtension($extension)
    {
        $this->twig->addExtension($extension);
    }

}