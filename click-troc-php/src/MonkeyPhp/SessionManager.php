<?php

namespace Framework;


use App\Entity\User;

class SessionManager
{
    public static function sessionStart($name, $limit = 0, $path = "/", $domain = null, $secure = null)
    {

        if (PHP_SESSION_ACTIVE !== session_status()) {
            session_name($name.'_Session');

            $https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);

            session_set_cookie_params($limit, $path, $domain, $https, true);
            session_start();


            if (self::validateSession())
            {
                if (!self::preventHijacking())
                {
                    $_SESSION = [];
                    $_SESSION['IPaddress'] = $_SERVER['REMOTE_ADDR'];
                    $_SESSION['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
                    self::regenerateSession();

                } elseif (rand(1, 100) <= 5) {
                    self::regenerateSession();
                }
            } else {
                $_SESSION = [];
                session_destroy();
                session_start();
            }
        }
    }

    static function regenerateSession()
    {
        if(isset($_SESSION['OBSOLETE']) && $_SESSION['OBSOLETE'] == true)
            return;

        $_SESSION['OBSOLETE'] = true;
        $_SESSION['EXPIRES'] = time() + 10000;

        session_regenerate_id(false);

        $newSession = session_id();
        session_write_close();

        session_id($newSession);
        session_start();

        unset($_SESSION['OBSOLETE']);
        unset($_SESSION['EXPIRES']);
    }

    static function validateSession()
    {
        if (isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES']) )
            return false;

        if (isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
            return false;

        return true;
    }

    protected static function preventHijacking()
    {
        if(!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent']))
            return false;

        if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
            return false;

        if( $_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
            return false;

        return true;
    }

    public static function clearSessions()
    {
        $_SESSION = [];
        session_destroy();
        session_start();
    }

    public static function clearSpecificSession($name)
    {
        unset($_SESSION[$name.'Session']);
    }

    public static function setUserConnected(User $user, $messageUnread)
    {
        $_SESSION['User'] = $user;
        $_SESSION['MessageUnread'] = $messageUnread;
    }

    public static function setSession($name, $value)
    {
        $_SESSION[$name] = $value;
    }
    public static function getUser()
    {
        return isset($_SESSION['User']) ? $_SESSION['User'] : null;
    }

    public static function stockHttpReferer($referer)
    {
        $_SESSION['RefererURI'] = $referer;
    }

    public static function addFlash($message, $type)
    {
        if (empty($_SESSION['flash'])) {
            $_SESSION['flash'] = [
                'message' => $message,
                'type' => $type
            ];
        }
    }

    public static function display($twig)
    {
        echo $twig->render("_partials/__flashMessage.html.twig");
        self::clearSpecificSession('flash');
    }

}