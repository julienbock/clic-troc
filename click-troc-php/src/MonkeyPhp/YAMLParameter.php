<?php


namespace App\MonkeyPhp;


use Symfony\Component\Yaml\Yaml;

class YAMLParameter
{

    protected $yamlFile;

    protected $env;

    public function __construct()
    {
        $this->getFileFromEnvironement();
    }

    public function getEnvironment()
    {
        $fileEnv =  Yaml::parseFile('../config/config.yaml');
        return $this->env = $fileEnv['env'];
    }

    public function getProtocol()
    {
        return $this->yamlFile['protocol'];
    }

    private function getFileFromEnvironement()
    {
        $this->getEnvironment();

        $this->yamlFile = (Yaml::parseFile('../config/config_'. $this->env .'.yaml'))['parameters'];
    }

    public function getDbAccess()
    {
        return $this->yamlFile['dbAccess'];
    }

    public static function getBaseUri()
    {
        $file = Yaml::parseFile('../config/config.yaml');

        return $file['base_uri'];
    }
}