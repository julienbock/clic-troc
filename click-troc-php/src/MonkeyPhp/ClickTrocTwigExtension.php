<?php

namespace Framework;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ClickTrocTwigExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [new TwigFunction('dump', 'dump'), new TwigFunction('')];
    }

}