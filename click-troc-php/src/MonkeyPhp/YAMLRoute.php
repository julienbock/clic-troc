<?php


namespace App\MonkeyPhp;


use App\Controller\AuthentificationController;
use App\MonkeyPhp\Router\RouteParser;
use App\Router\Router;
use Symfony\Component\Yaml\Yaml;

class YAMLRoute
{

    protected $yamlFile;

    static $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->getFileFromEnvironement();
    }

    public static function getRouteCollectionFromYAMLFile()
    {
        $routes = (Yaml::parseFile('../config/route.yaml'));

        $routeCollection = [];
        foreach ($routes as $nameRoute => $route) {
            $routeToCollection = new RouteParser();
            $routeToCollection
                ->setName($nameRoute)
                ->setMethod($route['method'])
                ->setCallable($route['callable'])
                ->setPath($route['path']);

            $routeCollection[] = $routeToCollection;
        }

        return $routeCollection;
    }
}