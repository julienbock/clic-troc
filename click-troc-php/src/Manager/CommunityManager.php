<?php

namespace App\Manager;

use App\Entity\City;
use App\Entity\Community;
use App\Request\AbstractRequest;
use App\Request\CityRequest;
use App\Request\CommunityRequest;

class CommunityManager extends AbstractRequest
{
    protected $communityRequest;

    protected $cityRequest;

    public function __construct()
    {
        $this->communityRequest = new CommunityRequest();
        $this->cityRequest = new CityRequest();
    }

    public function getAllCommunities()
    {
        $communities = $this->communityRequest->requestGetMostActiveCommunities();

        return $communities;
    }

    public function getCommunitiesByCity($cityId)
    {
        $communities = $this->communityRequest->findCommunityByCityId($cityId);

        return $communities;
    }

    public function findCity($name)
    {

        $cities = $this->cityRequest->findCityByName($name);

        $results = [];

        foreach ($cities as $city) {
            $results[] = new City($city);
        }

        return $results;

    }

    public function getCommunityById($idCommunity)
    {
        $community = $this->communityRequest->findCommunityById($idCommunity);

        return $community;
    }

    public function addUserToCommunity($idUser, $idCommunity)
    {
        $bool = $this->communityRequest->addUserInCommunity($idUser, $idCommunity);

        return $bool;
    }

    public function removeUserToCommunity($idUser, $idCommunity)
    {
        $bool = $this->communityRequest->removeUserInCommunity($idUser, $idCommunity);

        return $bool;
    }

    public function isAnUserMember($idUser, $idCommunity)
    {
        $bool = $this->communityRequest->isUserMember($idUser, $idCommunity);

        return $bool;
    }

    public function addCommunity($community)
    {
        return $this->communityRequest->addCommunity($community);
    }

    public function getCityById($id)
    {
        return $this->cityRequest->findCityById($id);
    }

    public function findCommunityByUser($idUser)
    {
        return $this->communityRequest->findCommunityByUser($idUser);
    }
}