<?php

namespace App\Manager;

use App\Entity\User;
use App\MonkeyPhp\YAMLWallet;
use App\Request\FriendshipsRequest;
use App\Request\MessageRequest;
use App\Request\UserRequest;
use Framework\Router;
use Framework\SessionManager;
use Framework\Upload;

class UserManager
{

    protected $ur;

    protected $mr;

    protected $fr;

    public function __construct()
    {
        $this->ur = new UserRequest();
        $this->mr = new MessageRequest();
        $this->fr = new FriendshipsRequest();
    }

    public function insertNewUser($table, Array $fields, array $value)
    {
        $userId = $this->ur->createNewUser($table, $fields, $value);
        YAMLWallet::createWalletYAML($userId);

        $image = new Upload('avatar');
        $image->startUpload($userId);

        if ($userId) {
            $_SESSION['username'] = $value['username'];
            $_SESSION['connected'] = true;
            $_SESSION['email'] = $value['email'];
            $_SESSION['firstname'] = $value['firstname'];
            $_SESSION['lastname'] = $value['lastname'];

            return true;
        } else {
            return false;
        }
    }

    public function checkIfUserExist($username, $password)
    {
        $resp = $this->ur->findUserByName($username);

        $user = null;
        if (!empty($resp)) {
            foreach ($resp as $community) {
                $user = new User($community);
            }
        }

        if (empty($user)) {
           return false;
        }

        if (password_verify($password, $user->getPassword())) {
            $messageUnread = $this->ur->findMessageUnreadUser($user->getId());
            SessionManager::setUserConnected($user, $messageUnread);

            return $user->getId();
        } else {
            return false;
        }

    }

    public function checkPaymentValidity($userId)
    {
        return $this->ur->checkPaymentValidityByUser($userId);
    }

    public function disablePaymentSubscription($subscriptionPaymentId)
    {
        return $this->ur->disablePaymentSubscription($subscriptionPaymentId);
    }

    public function checkIfMessageUnread()
    {
        if(SessionManager::getUser()) {
            $user = SessionManager::getUser();
            $messageUnread = $this->ur->findMessageUnreadUser($user->getId());
            SessionManager::clearSpecificSession('MessageUnread');
            SessionManager::setSession('MessageUnread',$messageUnread);
        }
    }

    public function findUnreadMessageByUser($idUser)
    {
        return $this->ur->findMessageUnreadUser($idUser);
    }

    public function validateUserProfilEdition($user, $informationsEdited)
    {
        return $this->ur->updateUserProfile($user->getId(), $informationsEdited);
    }

    public function findUserById($idUser, $getFriend = false)
    {
        return $this->ur->findUserById($idUser, $getFriend);
    }

    public function sendMessageToAnnounce($message)
    {
        return $this->mr->sendMessage($message);
    }

    public function sendAlertToUser($message)
    {
        return $this->mr->sendMessage($message);
    }

    public function updateNewMessageWithSameId($idMessageReturned)
    {
        return $this->mr->updateNewMessageWithSameId($idMessageReturned);
    }

    public function getPrivateMessagesByUser($idUser, $idMessages)
    {
        return $this->mr->findPrivateMessagesByUser($idUser, $idMessages);
    }

    public function getDiscussionByUserAndMessage($idUser, $idMessage)
    {
        return $this->mr->findDiscussionByUserAndMessage($idUser, $idMessage);
    }

    public function setReadedMessage($idUser, $idMessage)
    {
        return $this->mr->setReadedMessage($idUser, $idMessage);
    }

    public function findMessageById($idMessage)
    {
        return $this->mr->findMessageById($idMessage);
    }

    public function findIdDiscussion($idUser)
    {
        return $this->mr->findIdDiscussion($idUser);
    }

    public function findMessageUnreadUser($userId)
    {
        return $this->ur->findMessageUnreadUser($userId);
    }


    public function sendFriendRequest($idFriendSender, $idFriendRecever)
    {
        return $this->fr->insertRequestToFriend($idFriendSender, $idFriendRecever);
    }

    public function checkFriendship($idUser1, $currentUser)
    {
        return $this->ur->checkFriendship($idUser1, $currentUser);
    }

    public function replyFriendRequest($idFriendShip, $reply)
    {
        return $this->ur->replyFriendRequest($idFriendShip, $reply);
    }

    public function findUserByEmail($email)
    {
        return $this->ur->findUserByEmail($email);
    }

    public function addPayment($payment)
    {
        return $this->ur->addPayment($payment);
    }

}