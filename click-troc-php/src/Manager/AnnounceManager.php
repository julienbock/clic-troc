<?php

namespace App\Manager;

use App\Request\AnnounceRequest;
use App\Request\ProposalRequest;
use App\Request\ServiceRequest;

class AnnounceManager
{

    protected $ar;

    protected $sr;

    protected $pr;

    public function __construct()
    {
        $this->ar = new AnnounceRequest();
        $this->sr = new ServiceRequest();
        $this->pr = new ProposalRequest();
    }

    public function createAnnounceIntoSpecificallyCommunity($announce)
    {
        return $this->ar->insertAnnounce($announce);
    }

    public function getAnnouncesByCommunityId($idCommunity)
    {
        return $this->ar->getAnnouncesByCommunityId($idCommunity);
    }

    public function findAnnounceById($idAnnounce)
    {
        return $this->ar->getAnnounceById($idAnnounce);
    }

    public function getAnnouncesByUser($idUser)
    {
        return $this->ar->getAnnouncesByUser($idUser);
    }

    public function deleteAnnounceByIdAndUserId($idAnnounce, $idUser)
    {
        return $this->ar->deleteAnnounceByIdAndUserId($idAnnounce, $idUser);
    }

    public function findCityByAnnounce($idAnnounce, $idCommunity)
    {
        return $this->ar->findCityByAnnounce($idAnnounce, $idCommunity);
    }

    public function findImageByAnnounceId($idAnnounce)
    {
        return $this->sr->findImageByAnnounceId($idAnnounce);
    }

    public function adminDeleteAnnounce($idAnnounce, $idCommunity){
        return $this->ar->deleteAnnounceByIdAndCommunityId($idAnnounce, $idCommunity);
    }

    public function createNewProposal($proposal)
    {
        $fields = ['user_id_proposal', 'announce', 'created_at'];
        $values = [$proposal->getUserIdProposal(), $proposal->getAnnounce(), $proposal->getCreatedAt()];
        return $this->pr->createNewProposal('proposal', $fields, $values);
    }

    public function AcceptProposal($proposalId, $choiceValue)
    {
        return $this->pr->acceptProposal($proposalId, $choiceValue);
    }

    public function setWaitingExchange($announceId)
    {
        return $this->ar->setWaitingExchangeStatus($announceId);
    }

    public function setExchangedStatus($announceId)
    {
        return $this->ar->setExchangedStatus($announceId);
    }

    public function checkIfProposalAccepted($announceId)
    {
        return $this->pr->checkIfProposalAccepted($announceId);
    }

    public function setWaitingExchangeStatus($announceId)
    {
        return $this->ar->setWaitingExchangeStatus($announceId);
    }

    public function findProposalById($proposalId)
    {
        return $this->pr->findById($proposalId);
    }
    public function findProposalByAnnounceId($announceId)
    {
        return $this->pr->findByAnnounceIdAndAccepted($announceId);
    }
}