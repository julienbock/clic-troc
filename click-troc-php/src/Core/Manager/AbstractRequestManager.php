<?php

namespace App\Core\Manager;

use App\Request\AnnounceRequest;
use App\Request\CityRequest;
use App\Request\CommunityRequest;
use App\Request\MessageRequest;
use App\Request\ProposalRequest;
use App\Request\ServiceRequest;
use App\Request\UserRequest;

class AbstractRequestManager
{

    protected $ar;

    protected $cr;

    protected $cor;

    protected $mr;

    protected $sr;

    protected $ur;

    protected $pr;

    public function __construct()
    {
        $this->ar = new AnnounceRequest();
        $this->cr = new CityRequest();
        $this->cor = new CommunityRequest();
        $this->mr = new MessageRequest();
        $this->sr = new ServiceRequest();
        $this->ur = new UserRequest();
        $this->pr = new ProposalRequest();
    }

    /**
     * @return AnnounceRequest
     */
    public function getAr()
    {
        return $this->ar;
    }

    /**
     * @return CityRequest
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * @return CommunityRequest
     */
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * @return MessageRequest
     */
    public function getMr()
    {
        return $this->mr;
    }

    /**
     * @return ServiceRequest
     */
    public function getSr()
    {
        return $this->sr;
    }

    /**
     * @return UserRequest
     */
    public function getUr()
    {
        return $this->ur;
    }

    /**
     * @return ProposalRequest
     */
    public function getPr()
    {
        return $this->pr;
    }

    /**
     * @param ProposalRequest $pr
     */
    public function setPr($pr)
    {
        $this->pr = $pr;
    }
}