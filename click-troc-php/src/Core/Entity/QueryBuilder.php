<?php

namespace App\Core\Entity;

use App\MonkeyPhp\YAMLParameter;
use PDO;

class QueryBuilder
{

    protected $query;


    public function select($element)
    {
        $this->query = sprintf('SELECT %s',$element);
        return $this;

    }

    public function addSelect($elements)
    {
    }

    public function delete($element)
    {
        $this->query = sprintf('DELETE FROM %s',$element);
        return $this;

    }

    public function update($element)
    {
        $this->query = sprintf('UPDATE %s',$element);
        return $this;

    }

    public function join($table, $attribut)
    {
        $this->query = sprintf('%s JOIN %s ON %s.%s', $this->query, $table, $table, $attribut);

        return $this;

    }

    public function insert($table, Array $fields, Array $value)
    {
        $this->query = sprintf("INSERT into %s(%s) VALUES (%s)", $table, implode(", ", $fields),'"'.implode('","', $value).'"');

        return $this;
    }

    public function insertInteger($table, Array $fields, Array $value)
    {
        $this->query = sprintf("INSERT into %s(%s) VALUES (%s)", $table, implode(", ", $fields), implode(", ", array_map('intval', $value)));

        return $this;
    }

    public function leftJoin($table, $attribut)
    {
        $this->query = sprintf('%s LEFT JOIN %s ON %s.%s', $this->query, $table, $table, $attribut);

        return $this;

    }

    public function set($column, $value)
    {
        $this->query = sprintf('%s SET %s = "%s"', $this->query, $column, $value);

        return $this;
    }

    public function andSet($column, $value)
    {
        $this->query = sprintf('%s, %s = %s', $this->query, $column, $value);

        return $this;
    }

    public function setArray($array)
    {
        $i = 0;
        foreach ($array as $column => $value) {
            $i++;
            if ($i == 1) {
                $this->query = sprintf('%s SET %s = "%s"', $this->query, $column, $value);
            } else {
                $this->query = sprintf('%s ,%s = "%s"', $this->query, $column, $value);
            }
        }

        return $this;
    }

    public function from($table)
    {
        $this->query = sprintf('%s FROM %s', $this->query, $table);

        return $this;
    }

    public function where($name, $value)
    {
        $this->query = sprintf("%s WHERE %s = '%s'", $this->query, $name, $value);

        return $this;
    }

    public function andWhere($name, $value)
    {

        $this->query = sprintf("%s AND %s = '%s'", $this->query, $name, $value);
        return $this;
    }

    public function andWhereNotNull($name)
    {
        $this->query = sprintf('%s AND %s IS NOT null', $this->query, $name);
        return $this;
    }

    public function orWhere($name, $value)
    {
        $this->query = sprintf("%s OR %s = '%s'", $this->query, $name, $value);
        return $this;
    }

    public function whereIn($name, $array)
    {
        $in = [];
        foreach ($array as $item) {
            foreach($item as $k => $v) {
                if ($name == $k) {
                    $in[] = $v;
                }
            }
        }

        $this->query = sprintf("%s WHERE %s IN (%s)", $this->query, $name, '"'.implode('","',$in).'"');
        return $this;
    }

    public function columnEqual($column1, $column2)
    {
        $this->query = sprintf("%s AND %s = %s", $this->query, $column1, $column2);
        return $this;
    }

    public function orderBy($column, $sort = 'ASC')
    {
        $this->query = sprintf("%s ORDER BY %s %S", $this->query, $column, $sort);
        return $this;
    }

    public function groupBy($column)
    {
        $this->query = sprintf("%s GROUP BY %s ", $this->query, $column);
        return $this;
    }


    public function like($name, $value)
    {
        $this->query = $this->query .' WHERE '.$name." LIKE '".$value."%'";

        return $this;

    }

    public function limit($number)
    {
        $this->query = sprintf('%s LIMIT %s', $this->query, $number);
    }

    public function getResults()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        return $pdo->query($this->query);
    }

    public function execute()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->prepare($this->query);

        return $pdo->execute();
    }

    public function executeCount()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->prepare($this->query);

        return $pdo->execute();
    }

    public function fetch()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->query($this->query);

        return $pdo->fetch();
    }

    public function fetchColumn()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->query($this->query);

        return $pdo->fetchColumn();
    }

    public function fetchAll($class = null, $argts = [])
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->query($this->query);

        if ($class != null) {
            return $pdo->fetchAll(PDO::FETCH_CLASS, $class, $argts);
        } else {
            return $pdo->fetchAll();
        }
    }

    public function fetchObj($class, $argts = [])
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $pdo = $pdo->query($this->query);

        return $pdo->fetchObject($class, $argts);
    }

    public function executeInsert()
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        return $pdo->query($this->query);
    }

    public function getQuerySQL()
    {
        return $this->query;
    }

    public function executeAndReturnLastId($table)
    {
        $db = $this->newDbConnection();
        $pdo = $db->createPDO();

        $query = $pdo->prepare($this->query);

        $query->execute();

        return $pdo->lastInsertId($table);
    }

    public function addSQL($sql)
    {
        $this->query = sprintf('%s %s', $this->query, $sql);
        return $this;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    private function newDbConnection()
    {
        $yaml = new YAMLParameter();
        return new DataBase($yaml->getDbAccess());
    }

}