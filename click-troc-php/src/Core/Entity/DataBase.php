<?php

namespace App\Core\Entity;

use PDO;

class DataBase
{
    protected $dbHost;

    protected $dbName;

    protected $dbUser;

    protected $dbPassword;

    public function __construct($dbAccess)
    {
        $this->dbHost = $dbAccess['dbHost'];
        $this->dbName = $dbAccess['dbName'];
        $this->dbUser = $dbAccess['dbUser'];
        $this->dbPassword = $dbAccess['dbPass'];
    }

    /**
     * @return string
     */
    public function getDbHost(): string
    {
        return $this->dbHost;
    }

    /**
     * @param string $dbHost
     * @return DataBase
     */
    public function setDbHost(string $dbHost): DataBase
    {
        $this->dbHost = $dbHost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * @param mixed $dbName
     * @return DataBase
     */
    public function setDbName(string $dbName): DataBase
    {
        $this->dbName = $dbName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDbUser(): string
    {
        return $this->dbUser;
    }

    /**
     * @param string $dbUser
     * @return DataBase
     */
    public function setDbUser(string $dbUser): DataBase
    {
        $this->dbUser = $dbUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getDbPassword(): string
    {
        return $this->dbPassword;
    }

    /**
     * @param string $dbPassword
     * @return DataBase
     */
    public function setDbPassword(string $dbPassword): DataBase
    {
        $this->dbPassword = $dbPassword;
        return $this;
    }

    public function createPDO()
    {
        $pdo = new PDO(sprintf('mysql:host=%s;dbname=%s', $this->dbHost, $this->dbName), $this->dbUser, $this->dbPassword);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

}