<?php

namespace App\Controller;

use App\Router\Router;
use Framework\SessionManager;

abstract class AbstractController
{


    public function getManager($name)
    {
        $manager = sprintf('App\Manager\%s', $name);

        return new $manager();

    }

    public function checkIfUserConnected($router)
    {
        if (!SessionManager::getUser()) {
            SessionManager::addFlash('Vous n\'êtes pas connecté', 'error');
            $router->redirect('connexion.route', null, 301);
        }
    }
}