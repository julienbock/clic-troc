<?php

namespace App\Controller;

use App\Entity\Announce;
use App\Entity\Message;
use App\Entity\Community;
use App\Entity\Proposal;
use App\MonkeyPhp\YAMLWallet;
use App\Request\MessageRequest;
use App\Request\ServiceRequest;
use ArrayObject;
use DateTime;
use Framework\Router;
use Framework\SessionManager;
use Framework\Twig;
use Framework\Upload;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;

class DefaultController extends AbstractController
{

    protected $router;

    protected $twig;

    public function __construct(Router $router)
    {
        $this->twig = new Twig();
        $this->router = $router;
        $this->router->readRouteFromYAML($this);
        SessionManager::sessionStart('User');
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addGlobal('router', $router);
        $this->checkMessageNotifications();
    }

    public function homepage(ServerRequestInterface $request)
    {
        if (empty($_COOKIE['stepTunnelChecked'])) {
            echo $this->twig->render('stepTunnel.html.twig');
        } else {
            $findMostActiveCommunities = $this->getManager('CommunityManager')->getAllCommunities();
            echo $this->twig->render('homepage.html.twig', ['communities' => $findMostActiveCommunities]);
        }
    }

    public function listAllCommunities(ServerRequestInterface $request)
    {
        $communities = $this->getManager('CommunityManager')->getAllCommunities();

        echo $this->twig->render('communitiesList.html.twig', ['communities' => $communities]);
    }

    public function searchMotor(ServerRequestInterface $request)
    {
        $communities = $this->getManager('CommunityManager')->getCommunitiesByCity($request->getParsedBody()['search-value-city']);

        echo $this->twig->render('communitiesList.html.twig', ['communities' => $communities, 'cityName' => $request->getParsedBody()['search-value-input']]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function showCommunity(ServerRequestInterface $request)
    {
        $isAmember = false;

        $this->twig->addGlobal('request', $request);
        $idCommunity = $request->getAttribute('idCommunity');
        $community = $this->getManager('CommunityManager')->getCommunityById($idCommunity);

        $city = $this->getManager('CommunityManager')->getCityById($community->getCity());
        $announces = $this->getManager('AnnounceManager')->getAnnouncesByCommunityId($idCommunity);

        if (SessionManager::getUser()) {
            $idUser = (int)SessionManager::getUser()->getId();
            if (empty($idUser)) {
                return $this->router->redirect('connexion.route', null, 301);
            }
            $isAmember = boolval($this->getManager('CommunityManager')->isAnUserMember($idUser, $idCommunity));
        }

        echo $this->twig->render('community.html.twig', [
            'community' => $community,
            'isAMember' => $isAmember,
            'city' => $city,
            'announces' => $announces
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function adminCommunity(ServerRequestInterface $request){
        $communityId = $request->getAttribute('idCommunity');
        $announces = $this->getManager('AnnounceManager')->getAnnouncesByCommunityId($communityId);
        $community = $this->getManager('CommunityManager')->getCommunityById($communityId);
        echo $this->twig->render('adminCommunity.html.twig',[
            'announces' => $announces,
            'community' => $community
        ]);
    }

    public function ajaxAutoCompletionSearch(ServerRequestInterface $request)
    {

        $cities = $this->getManager('CommunityManager')->findCity($request->getParsedBody()['input']);

        $response = [];
        foreach ($cities as $city)
        {
            $response[] = ['id' => $city->getId(), 'name' => $city->getName()];
        }
        $response = json_encode($response);

        return new Response(200, ['content-type' => 'application/json'], $response);
    }

    public function ajaxJoinCommunity(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idCommunity = (int)$request->getParsedBody()['idCommunity'];
        $idUser = (int)$user->getId();

        $response = $this->getManager('CommunityManager')->addUserToCommunity($idUser, $idCommunity);

        return new Response(200, ['content-type' => 'application/json'], json_encode($response));
    }

    public function ajaxLeaveCommunity(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idCommunity = (int)$request->getParsedBody()['idCommunity'];
        $idUser = (int)$user->getId();

        $response = $this->getManager('CommunityManager')->removeUserToCommunity($idUser, $idCommunity);

        return new Response(200, ['content-type' => 'application/json'], json_encode($response));
    }

    public function addCommunity(ServerRequestInterface $request)
    {
        $this->checkIfUserConnected($this->router);

        echo $this->twig->render('addCommunity.html.twig');

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function validateAddCommunity(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }


        $community = new Community($request->getParsedBody());
        $community->setAdmin($user->getId());

        $response =  $this->getManager('CommunityManager')->addCommunity($community);

        $image = new Upload('community');

        if($_FILES)
        {
            $image->startUpload($response);
        }

        $idCommunity = (int)$response;
        $idUser = (int)SessionManager::getUser()->getId();
        $this->getManager('CommunityManager')->addUserToCommunity($idUser, $idCommunity);

        return $this->router->redirect('show.community', ['idCommunity' => $response], 301);
    }

    public function addAnnounce(ServerRequestInterface $request)
    {

        $idCommunity = $request->getAttribute('idCommunity');

        $this->checkIfUserConnected($this->router);

        echo $this->twig->render('addAnnounce.html.twig', ['idCommunity' => $idCommunity]);

        return new Response(200, ['content-type' => 'application/json'], null);

    }

    public function adminDeleteAnnounce(ServerRequestInterface $request){

        $this->checkIfUserConnected($this->router);

        $idAnnounce = $request->getAttribute('idAnnounce');
        $idCommunity = $request->getAttribute('idCommunity');
        $response = $this->getManager("AnnounceManager")->adminDeleteAnnounce($idAnnounce, $idCommunity);

        if ($response) {
            return $this->router->redirect('admin.community', ['idCommunity' => $idCommunity], 301);
        }

    }

    public function validateAddAnnounce(ServerRequestInterface $request)
    {
        $idCommunity = $request->getAttribute('idCommunity');

        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        if ($user) {
            $isAmember = boolval($this->getManager('CommunityManager')->isAnUserMember(SessionManager::getUser()->getId(), $idCommunity));

            if (!$isAmember) {
                SessionManager::addFlash('Vous ne faites pas parti de cette communauté', 'error');

                return $this->router->redirect('homepage', null, 301);
            }

            $announce = new Announce(true, $request->getParsedBody());
            $announce->setUserId(SessionManager::getUser()->getId());
            $announce->setCommunityId($idCommunity);
            $announce->setWaitingExchange((int)false);
            $announce->setWasExchanged((int)false);


            $response =  $this->getManager('AnnounceManager')->createAnnounceIntoSpecificallyCommunity($announce);

            $image = new Upload('announce');

            if($_FILES)
            {
                $image->startUpload($response);
            }
        }

        return $this->router->redirect('show.announce', ['idCommunity' => $idCommunity, 'idAnnounce' => $response], 301);
    }

    public function showAnnounce(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $isAmember = false;

        $idAnnounce = $request->getAttribute('idAnnounce');
        $idCommunity = $request->getAttribute('idCommunity');

        $isAmember = boolval($this->getManager('CommunityManager')->isAnUserMember($user->getId(), $idCommunity));


        $announce = $this->getManager('AnnounceManager')->findAnnounceById($idAnnounce);

        $imagesArray = [];

        $proposalAccepted = null;
        if ($announce->getAuthor()->getId() == $user->getId()) {
            $check = $this->getManager('AnnounceManager')->checkIfProposalAccepted($announce->getId());
            if ($check == 0) {
                $proposalAccepted = false;
            } else {
                $proposalAccepted = true;
            }
        }
        $images = $this->getManager('AnnounceManager')->findImageByAnnounceId($idAnnounce);
        foreach ($images as $image) {
            $imagesArray[] = '/uploads/announce/'.$image->getUploadName();
        }

        $city = $this->getManager('AnnounceManager')->findCityByAnnounce($idAnnounce, $announce->getCommunityId());

        echo $this->twig->render('announce.html.twig', [
            'announce' => $announce,
            'city' => $city,
            'imagesArray' => $imagesArray,
            'communityId' => $idCommunity,
            'checkProposalOneAccepted' => $proposalAccepted,
            'isMember' => $isAmember
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function deleteAnnounce(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idAnnounce = $request->getAttribute('idAnnounce');

        #$resp = $this->getManager('AnnounceManager')->deleteAnnounceByIdAndUserId($idAnnounce, $user->getId());

        $resp = "true";
        return new Response(200, ['content-type' => 'application/json'], json_encode($resp));
    }

    public function sendMessageToAnnounce(ServerRequestInterface $request)
    {
        $this->checkIfUserConnected($this->router);

        $idAnnounce = $request->getAttribute('idAnnounce');
        $idCommunity = $request->getAttribute('idCommunity');
        $announce = $this->getManager('AnnounceManager')->findAnnounceById($idAnnounce);

        $userAnnounce = $this->getManager('UserManager')->findUserById($announce->getUserId());

        echo $this->twig->render('sendMessageToAnnounce.html.twig', [
            'announce' => $announce,
            'userAnnounce' => $userAnnounce
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function checkSendMessageToAnnounce(ServerRequestInterface $request)
    {
        $datas = $request->getParsedBody();

        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idAnnounce = $request->getAttribute('idAnnounce');
        $announce = $this->getManager('AnnounceManager')->findAnnounceById($idAnnounce);
        $userAnnounce = $this->getManager('UserManager')->findUserById($announce->getUserId());

        $date = new DateTime();
        $message = new Message(true);
        $message
            ->setId2(0)
            ->setTitle($datas['title'])
            ->setMessage($datas['message'])
            ->setCreatedAt($date->format('Y-m-d H:i:s'))
            ->setSenderId($user->getId())
            ->setReceiverId($userAnnounce->getId())
            ->setSender($user->getId())
            ->setReceiver($userAnnounce->getId())
            ->setSenderRead(true)
            ->setReceiverRead(0)
            ->setAnnounceId($idAnnounce)
            ->setHasDiscussion(true)
            ->setHasAlert(false);

        $idMessageReturned = $this->getManager('UserManager')->sendMessageToAnnounce($message);

        $resp = $this->getManager('UserManager')->updateNewMessageWithSameId($idMessageReturned);

        $message->setId((int)$idMessageReturned);

        if ($resp) {
            if ($datas['makeProposal'] && $resp) {
                $this->makeProposalToExchange($message, $message->getId());
            }

            return $this->router->redirect('show.announce', ['idCommunity' => $announce->getCommunityId(), 'idAnnounce'  => $idAnnounce]);
        } else {
            return $this->router->redirect('send.message.announce', ['idCommunity' => $announce->getCommunityId(), 'idAnnounce'  => $idAnnounce]);
        }
    }

    public function makeProposalToExchange($discussion, $idDiscussion)
    {
        $user = SessionManager::getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $date = new DateTime();
        $messageProposal = new Message(false);

        $messageProposal
            ->setId2($idDiscussion)
            ->setTitle(null)
            ->setMessage($user->getUsername().' vous fait savoir qu\'il est intéréssé. Cliquer sur oui ou non')
            ->setCreatedAt($date->format('Y-m-d H:i:s'))
            ->setSenderId((int)$user->getId())
            ->setReceiverId((int)$discussion->getReceiverId())
            ->setSenderRead(true)
            ->setReceiverRead(0)
            ->setReceiver((int)$discussion->getReceiverId())
            ->setSender((int)$user->getId())
            ->setAnnounceId($discussion->getAnnounceId())
            ->setHasDiscussion(false)
            ->setHasAlert(false)
            ->setHasProposal(true);

        $proposal = new Proposal();
        $proposal
            ->setAnnounce((int)$messageProposal->getAnnounceId())
            ->setCreatedAt((new DateTime())->format('Y-m-d H:i:s'))
            ->setUserIdProposal((int)$messageProposal->getSenderId());

        $this->getManager('AnnounceManager')->createNewProposal($proposal);

        return $this->getManager('UserManager')->sendMessageToAnnounce($messageProposal);
    }

    public function AcceptOrDeclineProposal(ServerRequestInterface $request)
    {
        $choice = $request->getParsedBody();

        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idProposal = $request->getAttribute('idProposal');
        $proposal = $this->getManager('AnnounceManager')->findProposalById($idProposal);

        $choiceValue = null;
        if (array_key_exists('accept', $choice)) {
            $choiceValue = $choice['accept'];
            $this->getManager('AnnounceManager')->setWaitingExchange($proposal->getAnnounce());
        } else {
            $choiceValue = $choice['decline'];
        }

        $resp = $this->getManager('AnnounceManager')->AcceptProposal($idProposal, $choiceValue);

        if (!empty($resp)) {
            return $this->router->redirect('show.announce', ['idCommunity' => $proposal->community_id, 'idAnnounce' => $proposal->getAnnounce()], 301);
        }
    }

    public function announceWasExchangedAction(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idAnnounce = $request->getAttribute('idAnnounce');
        $announce = $this->getManager('AnnounceManager')->findAnnounceById($idAnnounce);
        $proposalAccepted = $this->getManager('AnnounceManager')->findProposalByAnnounceId($idAnnounce);
        if (!$announce->isWaitingExchange()) {
            return $this->router->redirect('profil.user', null, 301);
        }

        $this->getManager('AnnounceManager')->setExchangedStatus($idAnnounce);
        YAMLWallet::updateWalletYAML($announce->getAuthor()->getId(), 'echange', 0 + $announce->getUevValue());
        YAMLWallet::updateWalletYAML($proposalAccepted->getUserIdProposal(), 'echange', 0 - $announce->getUevValue());
        return $this->router->redirect('show.announce', ['idCommunity' => $announce->getCommunityId(),'idAnnounce' => $announce->getId()], 301);
    }

    public function showPrivateMessageBox()
    {
        $user = SessionManager::getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idMessages = $this->getManager('UserManager')->findIdDiscussion($user->getId());

        $messages = $this->getManager('UserManager')->getPrivateMessagesByUser($user->getId(), $idMessages);

        $test = ['toto', 'tata', 'tutu'];

        echo $this->twig->render('privateMessageBox.html.twig', [
            'messages' => $messages
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function showPrivateMessage(ServerRequestInterface $request)
    {
        $this->checkIfUserConnected($this->router);

        $idMessage = $request->getAttribute('idMessage');
        $user = $this->getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $this->checkMessageNotifications();
        $this->twig->addGlobal('session', $_SESSION);


        $messages = $this->getManager('UserManager')->getDiscussionByUserAndMessage($user->getId(), $idMessage);

        $proposal = null;
        if (!empty($messages[0]->getAnnounce())) {
            if ($messages[0]->getAnnounce()->getProposal()) {
                $proposal = $messages[0]->getAnnounce()->getProposal();
            }
        }

        $this->getManager('UserManager')->setReadedMessage($user->getId(), $idMessage);

        if ($messages[0]->getHasAlert()) {
            echo $this->twig->render('showAlertMessage.html.twig', [
                'messages' => $messages
            ]);
        } else {
            echo $this->twig->render('showPrivateMessage.html.twig', [
                'messages' => $messages,
                'proposal' => $proposal
            ]);

        }

        return new Response(200, ['content-type' => 'application/json'], null);

    }

    public function sendMessageToFriend(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idFriend = $request->getAttribute('idFriend');
        $friend = $this->getManager('UserManager')->findUserById($idFriend);


        echo $this->twig->render('sendMessageToFriend.html.twig', [
            'friend' => $friend
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function checkSendMessageToFriend(ServerRequestInterface $request)
    {
        $datas = $request->getParsedBody();

        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $idFriend = $request->getAttribute('idFriend');

        $date = new DateTime();
        $message = new Message(true);
        $message
            ->setId2(0)
            ->setTitle($datas['title'])
            ->setMessage($datas['message'])
            ->setCreatedAt($date->format('Y-m-d H:i:s'))
            ->setSenderId($user->getId())
            ->setReceiverId($idFriend)
            ->setSender($user->getId())
            ->setReceiver($idFriend)
            ->setSenderRead(true)
            ->setReceiverRead(0)
            ->setHasDiscussion(true)
            ->setHasAlert(false)
            ->setHasProposal(false);

        $idMessageReturned = $this->getManager('UserManager')->sendMessageToAnnounce($message);

        $resp = $this->getManager('UserManager')->updateNewMessageWithSameId($idMessageReturned);

        $message->setId((int)$idMessageReturned);

        return $this->router->redirect('show.private.message', ['idMessage' => $message->getId()]);
    }

    public function makeAnswerToMessage(ServerRequestInterface $request) {
        $datas = $request->getParsedBody();

        $idMessage = $request->getAttribute('idMessage');

        $discussion = $this->getManager('UserManager')->findMessageById($idMessage);

        $user = SessionManager::getUser();

        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $date = new DateTime();
        $answer = new Message(false);

        $titleMessage = array_key_exists("title", $datas) ? $datas['title'] : null;

        $answer = $this->createMessageObject(
            $discussion->getId(),
            $titleMessage,
            $datas['message'],
            $date->format('Y-m-d H:i:s'),
            $discussion->getSenderId(),
            true,
            $user->getId(),
            false,
            $discussion->getAnnounceId() ? null : $discussion->getAnnounceId(),
            true,
            false,
            false
            );

        $resp = $this->getManager('UserManager')->sendMessageToAnnounce($answer);

        if ($datas['makeProposal'] && $resp) {
            SessionManager::addFlash('Votre proposition a bien été envoyée', 'info');
            $this->makeProposalToExchange($discussion, $discussion->getId());
        } else {
            SessionManager::addFlash('Votre message a bien été envoyé', 'info');
        }

        if ($resp) {
            return $this->router->redirect('show.private.message', ['idMessage' => $answer->getId2()], 301);
        }
    }

    public function createMessageObject($discussionId = null, $title = null, $messageContent = null, $date, $receiverId, $receiverRead, $senderId, $senderRead, $announceId, $hasDiscussion = false, $hasAlert = false, $hasProposal = false)
    {
        $message = new Message();
        $message
            ->setId2($discussionId)
            ->setTitle($title)
            ->setMessage($messageContent)
            ->setCreatedAt($date)
            ->setSenderId((int)$senderId)
            ->setReceiverId((int)$receiverId)
            ->setSenderRead((int)$senderRead)
            ->setReceiverRead((int)$receiverRead)
            ->setAnnounceId($announceId)
            ->setHasDiscussion((int)$hasDiscussion)
            ->setHasAlert((int)$hasAlert)
            ->setHasProposal((int)$hasProposal);

        return $message;

    }

    public function checkMessageNotifications()
    {
        $this->getManager('UserManager')->checkIfMessageUnread();
    }

    public function getUser()
    {
       return SessionManager::getUser();
    }
}