<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 15/04/19
 * Time: 22:18
 */

namespace App\Controller;


use App\Entity\SubscriptionPayment;
use App\MonkeyPhp\YAMLParameter;
use App\MonkeyPhp\YAMLRoute;
use DateInterval;
use DateTime;
use Framework\Router;
use Framework\SessionManager;
use Framework\Twig;
use Framework\Upload;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Plasticbrain\FlashMessages\FlashMessages;
use Psr\Http\Message\ServerRequestInterface;
use Stripe\Checkout\Session as StripeCheckoutSession;
use Stripe\Stripe;

class AuthentificationController extends AbstractController
{

    protected $router;

    protected $twig;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->router->readRouteFromYAML($this);
        $this->twig = new Twig();
        SessionManager::sessionStart('User');
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addGlobal('router', $router);
    }


    public function connexion()
    {
        if (SessionManager::getUser()) {
            SessionManager::addFlash('Vous êtes déjà connecté', 'error');
            $this->router->redirect('homepage', null, 301);
        }

        $parsedUrl = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
        $fakeRequest = (new ServerRequest('get', parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH)));
        $findRoute = $this->router->match($fakeRequest);

        $referer = null;
        if(!empty($findRoute)) {
            $referer = $parsedUrl;
        } else {
            $referer = 'homepage';
        }
        SessionManager::stockHttpReferer($referer);

        echo $this->twig->render('connexion.html.twig');
    }

    public function connexionCheck(ServerRequestInterface $request)
    {
        $resp = $this->getManager('UserManager')->checkIfUserExist($request->getParsedBody()['username'], $request->getParsedBody()['password']);

        if($resp) {
            $currentDate = new DateTime();
            $user = $userAnnounce = $this->getManager('UserManager')->findUserById($resp);
            $subscriptionPayment = $this->getManager('UserManager')->checkPaymentValidity($user->getId());

            $expireAt = new DateTime($subscriptionPayment->getExpireAt());
            if (!$subscriptionPayment || $expireAt < $currentDate) {
                SessionManager::clearSessions();
                $this->getManager('UserManager')->disablePaymentSubscription($subscriptionPayment->getId());
                $this->redirectToStripePayment(null, $user->getEmail());
            }

            return (new Response())->withStatus(301)->withHeader('Location', YAMLParameter::getBaseUri().$_SESSION['RefererURI']);
        } else {
            return $this->router->redirect('connexion.route', null, 301);
        }
    }

    public function inscription()
    {
        echo $this->twig->render('signin.html.twig');
    }

    public function inscriptionCheck(ServerRequestInterface $request)
    {
        $fields = ['username','lastname','firstname','email', 'password'];
        $value = $request->getParsedBody();
        $value['password'] = password_hash($value['password'], PASSWORD_DEFAULT);
        unset($value['password-confirm']);
        unset($value['file']);
        $response = $this->getManager('UserManager')->insertNewUser('user', $fields, $value);

        if($response) {
            return $this->router->redirect('check.inscription.redirect.stripe.payment', null, 301);
        } else {
            return $this->router->redirect('inscription.route', null, 301);
        }
    }

    public function redirectToStripePayment(ServerRequestInterface $request = null, $email = null)
    {
        Stripe::setApiKey('sk_test_0kCs9Nh37S4iKJasi2BSaC1Z00RRehe4r2');

        $userEmail = null;
        if (!empty($email)) {
            $userEmail = $email;
        } else {
            $userEmail = $_SERVER['email'];
        }

        $stripeSessionCheckout = StripeCheckoutSession::create([
            'payment_method_types' => ['card'],
            'customer_email' => $userEmail,
            'subscription_data' => [
                'items' => [[
                    'plan' => 'plan_FIvkpFpkI8KRoV'
                ]],
            ],
            'success_url' => YAMLParameter::getBaseUri().$this->router->generateUri('stripe.response').'?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => YAMLParameter::getBaseUri().$this->router->generateUri('stripe.response'),
        ]);

        echo $this->twig->render('redirectToPayment.html.twig', ['stripeCheckoutSession' => $stripeSessionCheckout]);
    }

    public function stripeResponseStatus(ServerRequestInterface $request)
    {
        $date = new DateTime();
        $expireDate = (new DateTime())->add(new DateInterval('P1Y'));
        if (array_key_exists('session_id', $request->getQueryParams())) {
            Stripe::setApiKey('sk_test_0kCs9Nh37S4iKJasi2BSaC1Z00RRehe4r2');
            $checkout = StripeCheckoutSession::retrieve($request->getQueryParams()['session_id']);
            $user = $this->getManager('UserManager')->findUserByEmail($checkout->customer_email);
            $payment = new SubscriptionPayment();
            $payment
                ->setActive(1)
                ->setPayedAt($date->format('d-m-y H:i:s'))
                ->setExpireAt($expireDate->format('d-m-y H:i:s'))
                ->setUserId($user->getId());

            $this->getManager('UserManager')->addPayment($payment);
            $messageUnread = $this->getManager('UserManager')->findMessageUnreadUser($user->getId());
            SessionManager::setUserConnected($user, $messageUnread);

            return $this->router->redirect('profil.user', null, 301);
        } else {
            return $this->router->redirect('profil.user', null, 301);
        }
    }

    public function logout(ServerRequestInterface $request)
    {
        SessionManager::clearSessions();
        SessionManager::addFlash('Vous vous êtes déconnecté', 'error');

        return $this->router->redirect('connexion.route', null, 301);
    }
}