<?php

namespace App\Controller;

use App\Entity\Message;
use App\MonkeyPhp\YAMLWallet;
use DateTime;
use Framework\Router;
use Framework\SessionManager;
use Framework\Twig;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends AbstractController
{
    protected $router;

    protected $twig;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->router->readRouteFromYAML($this);
        $this->twig = new Twig();
        SessionManager::sessionStart('User');
        $this->twig->addGlobal('session', $_SESSION);
        $this->twig->addGlobal('router', $router);
    }

    public function profilUser()
    {
        $this->checkIfUserConnected($this->router);
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $user = $this->getManager('UserManager')->findUserById($user->getId(), true);

        $walletHisto = null;
        $walletTotal = null;
        $walletHisto = YAMLWallet::readWalletYAML($user->getId());
        $walletTotal = YAMLWallet::getLastTotalWallet($user->getId());

        $announces = $this->getManager('AnnounceManager')->getAnnouncesbyuser($user->getId());
        $communities = $this->getManager('CommunityManager')->findCommunityByUser($user->getId());

        echo $this->twig->render('profil.html.twig', [
            'user' => $user,
            'announces' => $announces,
            'communities' => $communities,
            'walletHisto' => $walletHisto,
            'walletTotal' => $walletTotal
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function showProfilUser(ServerRequestInterface $request)
    {
        $idUser = $request->getAttribute('idUser');

        $currentUser = SessionManager::getUser();

        if (empty($currentUser)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        $userProfil = $this->getManager('UserManager')->findUserById($idUser, true);

        $friendshipStatus = $this->getManager('UserManager')->checkFriendship($idUser, $currentUser->getId());

        $announces = $this->getManager('AnnounceManager')->getAnnouncesbyuser($userProfil->getId());
        $communities = $this->getManager('CommunityManager')->findCommunityByUser($userProfil->getId());

        echo $this->twig->render('profil.html.twig', [
            'user' => $userProfil,
            'friendships' => $friendshipStatus,
            'announces' => $announces,
            'communities' => $communities,
        ]);

        return new Response(200, ['content-type' => 'application/json'], null);
    }

    public function editProfilUser()
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }

        echo $this->twig->render('editProfil.html.twig', ['user' => $user]);
    }

    public function validateProfilUserEdition(ServerRequestInterface $request)
    {
        $user = SessionManager::getUser();
        if (empty($user)) {
            return $this->router->redirect('connexion.route', null, 301);
        }
        $informationsEdited= $request->getParsedBody();

        $this->getManager('UserManager')->validateUserProfilEdition($user, $informationsEdited);

        $user = $this->getManager('UserManager')->findUserById($user->getId());
        $messageUnread = $this->getManager('UserManager')->findUnreadMessageByUser($user->getId());

        SessionManager::setUserConnected($user, $messageUnread);

        return $this->router->redirect('profil.user', null, 301);
    }

    public function sendFriendRequest(ServerRequestInterface $request)
    {
        $currentUser = SessionManager::getUser();
        if (empty($currentUser)) {
            return $this->router->redirect('connexion.route', null, 301);
        }
        $userTargetId = $request->getAttribute('idUser');

        $date = new DateTime();

        $resultInsert = $this->getManager('UserManager')->sendFriendRequest($currentUser->getId(), $userTargetId);

        $messageInfo = new Message();
        $messageInfo
            ->setId2(0)
            ->setTitle("Nouvelle demande d''ami")
            ->setMessage("Vous avez reçu une nouvelle demande d''ami, vous pouvez l''accepter ou la refuser. Le fait d''avoir des personnes en ami vous permettra de retrouver plus facilement vos contacts et de créer votre propre réseau de troqueur.")
            ->setSenderId($currentUser->getId())
            ->setReceiverId($userTargetId)
            ->setSenderRead(true)
            ->setReceiverRead(0)
            ->setFriendshipId($resultInsert)
            ->setHasDiscussion(false)
            ->setHasAlert(true)
            ->setCreatedAt($date->format('Y-m-d H:i:s'));

        $idReturned = $this->getManager('UserManager')->sendAlertToUser($messageInfo);

        $resp = $this->getManager('UserManager')->updateNewMessageWithSameId($idReturned);

        if ($resp) {
            return $this->router->redirect('show.profil.user', ['idUser' => $userTargetId], 301);
        }
    }

    public function replyFriendRequest(ServerRequestInterface $request)
    {
        $idFrienship = $request->getAttribute('idFriendship');
        $idUserSendRequest = $request->getAttribute('idUser');

        //convert string to a boolean
        $reply = filter_var($request->getParsedBody()['reply'], FILTER_VALIDATE_BOOLEAN);

        $resp = $this->getManager('UserManager')->replyFriendRequest($idFrienship, (int)$reply);

        if($resp) {
            return $this->router->redirect('show.profil.user', ['idUser' => $idUserSendRequest], 301);
        }
    }

}