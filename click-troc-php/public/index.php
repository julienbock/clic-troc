<?php

require __DIR__ . "/../vendor/autoload.php";

use App\Controller\AuthentificationController;
use App\Controller\UserController;
use Framework\App;
use App\Controller\DefaultController;
use GuzzleHttp\Psr7\ServerRequest;

$app = new App(
    [DefaultController::class, AuthentificationController::class, UserController::class]
);

$response = $app->run(ServerRequest::fromGlobals());
\Http\Response\send($response);
