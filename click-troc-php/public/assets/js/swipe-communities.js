// 1. Basic object for our stuff
window.sliderCommunities = {};

// 2. Settings
sliderCommunities.sliderPanelSelector = '.slider-communities-panel';
sliderCommunities.sliderPaginationSelector = '.slider-communities-pagination';
sliderCommunities.sensitivity = 5; // horizontal % needed to trigger swipe

// 2. Placeholder to remember which slide we’re on
sliderCommunities.activeSlide = 0;

// 3. Slide counter
sliderCommunities.slideCount = 0;

// 4. Initialization + event listener
sliderCommunities.init = function( selector ) {

    // 4a. Find the container
    sliderCommunities.sliderEl = document.querySelector( selector );

    // 4b. Count stuff
    sliderCommunities.slideCount = sliderCommunities.sliderEl.querySelectorAll( sliderCommunities.sliderPanelSelector ).length;

    // 4c. Populate pagination
    var n = 0;
    for( n; n < sliderCommunities.slideCount; n++ ) {
        var activeStatus = n == sliderCommunities.activeSlide ? ' class="is-active"' : '';
        sliderCommunities.sliderEl.parentElement.querySelector( sliderCommunities.sliderPaginationSelector ).innerHTML += '<div ' + activeStatus + '></div>';
    }

    // 4d. Set up HammerJS
    var sliderManager = new Hammer.Manager( sliderCommunities.sliderEl );
    sliderManager.add( new Hammer.Pan({ threshold: 0, pointers: 0 }) );
    sliderManager.on( 'pan', function( e ) {

        // 4e. Calculate pixel movements into 1:1 screen percents so gestures track with motion
        var percentage = 100 / sliderCommunities.slideCount * e.deltaX / window.innerWidth;

        // 4f. Multiply percent by # of slide we’re on
        var percentageCalculated = percentage - 100 / sliderCommunities.slideCount * sliderCommunities.activeSlide;

        // 4g. Apply transformation
        sliderCommunities.sliderEl.style.transform = 'translateX( ' + percentageCalculated + '% )';

        // 4h. Snap to slide when done
        if( e.isFinal ) {
            if( e.velocityX > 1 ) {
                sliderCommunities.goTo( sliderCommunities.activeSlide - 1 );
            } else if( e.velocityX < -1 ) {
                sliderCommunities.goTo( sliderCommunities.activeSlide + 1 )
            } else {
                if( percentage <= -( sliderCommunities.sensitivity / sliderCommunities.slideCount ) )
                    sliderCommunities.goTo( sliderCommunities.activeSlide + 1 );
                else if( percentage >= ( sliderCommunities.sensitivity / sliderCommunities.slideCount ) )
                    sliderCommunities.goTo( sliderCommunities.activeSlide - 1 );
                else
                    sliderCommunities.goTo( sliderCommunities.activeSlide );
            }
        }
    });
};

// 5. Update current slide
sliderCommunities.goTo = function( number ) {

    // 5a. Stop it from doing weird things like moving to slides that don’t exist
    if( number < 0 )
        sliderCommunities.activeSlide = 0;
    else if( number > sliderCommunities.slideCount - 1 )
        sliderCommunities.activeSlide = sliderCommunities.slideCount - 1;
    else
        sliderCommunities.activeSlide = number;

    // 5b. Apply transformation & smoothly animate via .is-animating CSS
    sliderCommunities.sliderEl.classList.add( 'is-animating' );
    var percentage = -( 100 / sliderCommunities.slideCount ) * sliderCommunities.activeSlide;
    sliderCommunities.sliderEl.style.transform = 'translateX( ' + percentage + '% )';
    clearTimeout( sliderCommunities.timer );
    sliderCommunities.timer = setTimeout( function() {
        sliderCommunities.sliderEl.classList.remove( 'is-animating' );
    }, 400 );

    // 5c. Update the counters
    var pagination = sliderCommunities.sliderEl.parentElement.querySelectorAll( sliderCommunities.sliderPaginationSelector + ' > *' );
    var n = 0;
    for( n; n < pagination.length; n++ ) {
        var className = n == sliderCommunities.activeSlide ? 'is-active' : '';
        pagination[n].className = className;
    }
};

// Initialize
sliderCommunities.init( '#slider-communities' );