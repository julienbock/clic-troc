/*
  Slidemenu come to left
*/
(function() {
    var $body = document.body
        , $menu_trigger = $body.getElementsByClassName('menu-slide-big')[0];

    var $content_trigger = $body.getElementsByClassName('container-trigger')[0];

    if ( typeof $menu_trigger !== 'undefined' ) {
        $menu_trigger.addEventListener('click', function() {
            $body.className = ( $body.className == 'menu-active' )? '' : 'menu-active';
        });
    }

    if ( typeof $content_trigger !== 'undefined') {
        $content_trigger.addEventListener('click', function () {
                if ($body.className == 'menu-active') {
                    $body.className = '';
                }
            })
    }

    }).call(this);


function joinCommunityActionAjax(idCommunity){
    $.ajax({
        url: "/ajax-join-community",
        type: "POST",
        data: {'idCommunity' : idCommunity},
        success:
            function (response) {
                if(response === true) {
                    $('#join-community-action').hide();
                   $('#leave-community-action').show();
                   $('#add-announce-action').show();
                }
            }
    });
}

function leaveCommunityActionAjax(idCommunity){
    $.ajax({
        url: "/ajax-leave-community",
        type: "POST",
        data: {'idCommunity' : idCommunity},
        success:
            function (response) {
                if (response === true) {
                    $('#join-community-action').show();
                    $('#leave-community-action').hide();
                    $('#add-announce-action').hide();
                }
            }
    });
}

function deleteAnnounceFromProfilPanel(idannounce){

}

function showLocalisationChoice(){
    var localisation = document.getElementsByClassName('localisation');
    if (localisation[0].style.display === "block") {

        localisation[0].style.display = "none";

    } else {
        localisation[0].style.display = "block";
    }
}

function redirectionToAnnounce(idcommunity, idannounce){
    document.location.href="/communaute/"+ idcommunity + "/annonce/" + idannounce;
}


function setCookie() {
    let now = new Date();
    let time = now.getTime();
    time += 3600 * 1000 * 24 * 365 * 10;
    now.setTime(time);
    let value = 'true';
    document.cookie =
        'stepTunnelChecked=' + value +
        '; expires=' + now.toUTCString() +
        '; path=/';
    location.reload();
}

$(function() {
    $('#search-input-hash').on('focus', function() {
        $('html, body').animate({
            scrollTop: $(".footer-content").offset().top
        }, 2000);
    });

    $('.flash-close-button').on('click', function() {
       $('.flash-wrap').hide();
    });

    $("#search-input-hash").on('keyup', function() {
        var resultList = $('#list-result-block');
        var n = $(this).val().length;
        if (n >= 3) {
            $.ajax({
                url: "/ajax-autocompletion",
                type: "POST",
                data: {'input': $(this).val()},
                success:
                    function (response) {
                        resultList.empty();
                        $('#list-result').show();
                        let count = 0;
                        $.each(response, (e, v) => {
                            count++;
                            resultList.append('<li class="list-element element-' + count + '" data-cityid="' + v.id + '" data-cityname="' + v.name +'">' + v.name + '</li>').data('city', v.name);
                        });

                        $('.list-element').on('click', function() {
                            $("#search-input-hash").val($(this).data('cityname'));
                            $('#search-input-city2').val($(this).data('cityid'));
                            $('#list-result').hide();
                        });
                    }
            });
        }
        if ( n < 3) {
            $('#list-result').hide();
        }
    });

    $(".btn-delete-announce").on('click', function() {
        var idAnnounce = $(this).data('announce');
        $.ajax({
            url: "/suppression-annonce/" + idAnnounce,
            type: "POST",
            success:
                function (response) {
                    if (response === 'true') {
                        $(this).parent().parent().remove();
                    }
                }
        });
    });


});

$(document).mouseup(function(e){
    var container = $("#list-result");

    // If the target of the click isn't the container
    if(!container.is(e.target) && container.has(e.target).length === 0){
        container.hide();
    }
});

let abc = 1;      // Declaring and defining global increment variable.
$(document).ready(function() {
//  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
    $('#add_more').click(function() {
        abc += 1;
        $(this).before($("<div/>", {
            id: 'filediv' + abc,
            class: 'file-content'
        }).fadeIn('slow').append($("<input/>", {
            name: 'file[]',
            type: 'file',
            class: 'input-img btn-principal',
            id: 'file' + abc
        }), $("<br/><br/>")).append($("<label/>", {
            for: 'file' + abc,
            class: 'btn-principal',
            id: 'select-img',
            text:'Selectionner une image'
        }), $("</label><br/><br/>")));
    });
// Following function will executes on change event of file input to select different file.
    $(document).on('change', '.input-img' , function() {
        if (this.files && this.files[0]) {
            var z = abc - 1;
            var filediv = $(this).parent().parent().find('#filediv' + abc);
            var currentElement = filediv.find('#file' + abc);
            var x = currentElement.find('#previewimg' + z).remove();
            currentElement.before("<div id='abcd" + abc + "' class='abcd'><img  class='thumbnail-resize' id='previewimg" + abc + "' src=''/></div>");
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            currentElement.hide();
            filediv.find('#select-img').remove();
            $("#abcd" + abc).append($("<a/>", {
                id: 'img',
                class: 'btn-principal btn-principal-red',
                alt: 'delete',
                text: 'supprimer'
            }).click(function() {
                abc += 1;
                if($('#button-sign-up').show()) {
                    $('#button-sign-up').before($("<div/>", {
                        id: 'filediv' + abc,
                        class: 'file-content'
                    }).fadeIn('slow').append($("<input/>", {
                        name: 'file[]',
                        type: 'file',
                        class: 'input-img btn-principal',
                        id: 'file' + abc
                    }), $("<br/><br/>")).append($("<label/>", {
                        for: 'file' + abc,
                        class: 'btn-principal',
                        id: 'select-img',
                        text: 'Selectionner une image'
                    }), $("</label><br/><br/>")));
                    currentElement.parent().remove();
                }
            }));
        }
    });
// To Preview Image
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };
    $('#upload').click(function(e) {
        var name = $(":file").val();
        if (!name) {
            alert("Une image doit être choisie");
            e.preventDefault();
        }
    });
});