// 1. Basic object for our stuff
window.sliderFriends = {};

// 2. Settings
sliderFriends.sliderPanelSelector = '.slider-friends-panel';
sliderFriends.sliderPaginationSelector = '.slider-friends-pagination';
sliderFriends.sensitivity = 5; // horizontal % needed to trigger swipe

// 2. Placeholder to remember which slide we’re on
sliderFriends.activeSlide = 0;

// 3. Slide counter
sliderFriends.slideCount = 0;

// 4. Initialization + event listener
sliderFriends.init = function( selector ) {

    // 4a. Find the container
    sliderFriends.sliderEl = document.querySelector( selector );

    // 4b. Count stuff
    sliderFriends.slideCount = sliderFriends.sliderEl.querySelectorAll( sliderFriends.sliderPanelSelector ).length;

    // 4c. Populate pagination
    var n = 0;
    for( n; n < sliderFriends.slideCount; n++ ) {
        var activeStatus = n == sliderFriends.activeSlide ? ' class="is-active"' : '';
        sliderFriends.sliderEl.parentElement.querySelector( sliderFriends.sliderPaginationSelector ).innerHTML += '<div ' + activeStatus + '></div>';
    }

    // 4d. Set up HammerJS
    var sliderManager = new Hammer.Manager( sliderFriends.sliderEl );
    sliderManager.add( new Hammer.Pan({ threshold: 0, pointers: 0 }) );
    sliderManager.on( 'pan', function( e ) {

        // 4e. Calculate pixel movements into 1:1 screen percents so gestures track with motion
        var percentage = 100 / sliderFriends.slideCount * e.deltaX / window.innerWidth;

        // 4f. Multiply percent by # of slide we’re on
        var percentageCalculated = percentage - 100 / sliderFriends.slideCount * sliderFriends.activeSlide;

        // 4g. Apply transformation
        sliderFriends.sliderEl.style.transform = 'translateX( ' + percentageCalculated + '% )';

        // 4h. Snap to slide when done
        if( e.isFinal ) {
            if( e.velocityX > 1 ) {
                sliderFriends.goTo( sliderFriends.activeSlide - 1 );
            } else if( e.velocityX < -1 ) {
                sliderFriends.goTo( sliderFriends.activeSlide + 1 )
            } else {
                if( percentage <= -( sliderFriends.sensitivity / sliderFriends.slideCount ) )
                    sliderFriends.goTo( sliderFriends.activeSlide + 1 );
                else if( percentage >= ( sliderFriends.sensitivity / sliderFriends.slideCount ) )
                    sliderFriends.goTo( sliderFriends.activeSlide - 1 );
                else
                    sliderFriends.goTo( sliderFriends.activeSlide );
            }
        }
    });
};

// 5. Update current slide
sliderFriends.goTo = function( number ) {

    // 5a. Stop it from doing weird things like moving to slides that don’t exist
    if( number < 0 )
        sliderFriends.activeSlide = 0;
    else if( number > sliderFriends.slideCount - 1 )
        sliderFriends.activeSlide = sliderFriends.slideCount - 1;
    else
        sliderFriends.activeSlide = number;

    // 5b. Apply transformation & smoothly animate via .is-animating CSS
    sliderFriends.sliderEl.classList.add( 'is-animating' );
    var percentage = -( 100 / sliderFriends.slideCount ) * sliderFriends.activeSlide;
    sliderFriends.sliderEl.style.transform = 'translateX( ' + percentage + '% )';
    clearTimeout( sliderFriends.timer );
    sliderFriends.timer = setTimeout( function() {
        sliderFriends.sliderEl.classList.remove( 'is-animating' );
    }, 400 );

    // 5c. Update the counters
    var pagination = sliderFriends.sliderEl.parentElement.querySelectorAll( sliderFriends.sliderPaginationSelector + ' > *' );
    var n = 0;
    for( n; n < pagination.length; n++ ) {
        var className = n == sliderFriends.activeSlide ? 'is-active' : '';
        pagination[n].className = className;
    }
};

// Initialize
sliderFriends.init( '#slider-friends' );